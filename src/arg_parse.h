/**
 * @file
 * Internal header file for arg_parse.c function prototypes.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/*
 * Internal header file for use in watchdog server, containing non-static
 * function prototypes, global variable declarations, and convenience macros.
 */

#ifndef ARG_PARSE_H
#define ARG_PARSE_H

int parse_timeout_arg(unsigned long *timeout, const char *arg);

#endif /* ARG_PARSE_H */
