/**
 * @file
 * Helper functions for handling monitor internal files.
 *
 * Monitor files are placed directly in the interface directory.
 *
 * List of monitor files used:
 *
 * .R.I.P.   - Used to indicate that monitor state is not UP.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"

#include <stdio.h>
#include <errno.h>
#include <limits.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "watchdog_int.h"

/**
 * Get path to monitor file.
 *
 * Construct absolute path to monitor file, checking for errors and
 * truncation.
 *
 * Subsequent calls to monitor_file_path() will invalidate the string from
 * previous call.
 *
 * @param file_name Name of monitor file.
 * @returns Pointer to statically allocated string containing absolute path to
 *   monitor file, or NULL on error.
 */
static const char * monitor_file_path(const char *file_name)
{
    static char path[PATH_MAX];
    int path_len;
    path_len = snprintf(path, sizeof(path), "%s/%s", interface_dir, file_name);
    if (path_len < 0) {
        fprintf(stderr, "%s: snprintf failed\n", __func__);
        return NULL;
    } else if (path_len >= sizeof(path)) {
        fprintf(stderr, "%s: snprintf was truncated\n", __func__);
        return NULL;
    }
    return path;
}

/**
 * Check for monitor file in interface_directory.
 *
 * Error messages are printed to stderr in case of errors while checking for
 * the file.
 *
 * @param file_name Name of monitor file.
 * @returns 0 if file is not found, 1 if file is found, -1 on error.
 */
int monitor_file_exists(const char *file_name)
{
    const char *monitor_file;
    struct stat sb;

    /* Get path to file */
    monitor_file = monitor_file_path(file_name);
    if (monitor_file == NULL) {
        fprintf(stderr, "%s: monitor_file_path failed\n", __func__);
        return -1;
    }

    /* Get file status */
    if (stat(monitor_file, &sb) == -1) {
        if (errno == ENOENT) {
            /* File not found */
            return 0;
        }
        /* Something else failed */
        perror("stat");
        return -1;
    }

    return 1;
}

/**
 * Write empty file in interface directory.
 *
 * Error messages are printed to stderr in case of errors while checking for
 * the file.
 *
 * @param file_name Name of monitor file.
 * @returns 0 on success, -1 on error.
 */
int monitor_file_write(const char *file_name)
{
    const char *monitor_file;
    int fd;

    /* Get path to file */
    monitor_file = monitor_file_path(file_name);
    if (monitor_file == NULL) {
        fprintf(stderr, "%s: monitor_file_path failed\n", __func__);
        return -1;
    }

    /* Create or truncate and touch the file */
    fd = open(monitor_file, O_WRONLY|O_CREAT|O_TRUNC, 0644);
    if (fd == -1) {
        perror("open");
        return -1;
    }
    if (close(fd))
        perror("close");        /* Don't fail, just log error */

    return 0;
}
