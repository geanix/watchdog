/**
 * @file
 * Watchdog client API implementation.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "watchdog.h"

/** Interface directory path. */
static const char *interface_dir = DEFAULT_INTERFACE_DIR;

/**
 * Open client directory.
 * @param name Client name.
 * @returns File descriptor to client direcotry on success, -1 on error.
 */
static int client_dir(const char *name)
{
    char *path = NULL;
    struct stat sb;
    int fd = -1;

    /* Get path to client interface dir */
    path = malloc(strlen(interface_dir) + strlen(name) + 2);
    if (path == NULL) {
        fputs("malloc: Out of memory\n", stderr);
        return -1;
    }
    sprintf(path, "%s/%s", interface_dir, name);

    /* Ensure directory path exists */
    if (stat(path, &sb) == -1) {
        switch (errno) {
        case ENOENT:
            /* Create directory if it does not exist */
            if (mkdir(path, 0755) == -1) {
                fprintf(stderr, "mkdir: %s: %s\n", path, strerror(errno));
                goto out;
            }
            break;
        default:
            fprintf(stderr, "stat: %s: %s\n", path, strerror(errno));
            goto out;
        }
    }

    /* Open fd */
    fd = open(path, O_RDONLY|O_DIRECTORY|O_CLOEXEC);
    if (fd == -1) {
        fprintf(stderr, "open: %s: %s\n", path, strerror(errno));
        goto out;;
    }

out:
    free(path);
    /* Return fd handle to directory */
    return fd;                 /* Keep in mind that fd is initialized to -1 */
}

int watchdog_register(const char *name, unsigned start_timeout,
                      unsigned keepalive_timeout,
                      const char *recover_cmd, unsigned recover_timeout)
{
    int dirfd = -1, tmpfd = -1, retval = -1;
    char path[PATH_MAX];

    /* Get handle to client interface directory */
    dirfd = client_dir(name);
    if (dirfd == -1) {
        fprintf(stderr, "%s: Could not open directory\n", __func__);
        return -1;
    }

    /* Write registration file as an unnamed temporary file */
    tmpfd = openat(dirfd, ".", O_WRONLY|O_TMPFILE|O_CLOEXEC, 0644);
    if (tmpfd == -1) {
        perror("openat");
        fprintf(stderr, "%s: Could not create temporary file\n", __func__);
        goto out;
    }
    if (dprintf(tmpfd,
                "start_timeout=%ums\n"
                "keepalive_timeout=%ums\n",
                start_timeout, keepalive_timeout)
        < 0) {
        fprintf(stderr, "%s: Error writing to temporary file\n", __func__);
        goto out;
    }
    if (recover_cmd && dprintf(tmpfd,
                               "recover_cmd=%s\n"
                               "recover_timeout=%ums\n",
                               recover_cmd, recover_timeout)
        < 0) {
        fprintf(stderr, "%s: Error writing to temporary file\n", __func__);
        goto out;
    }

    /* Create directory entry for registration file.
     *
     * It is necessary to allow repeated calls of watchdog_register() without
     * intermediate watchdog_unregister(), as the start_timeout would otherwise
     * not work as expected (it would be temporary cancelled while in READY
     * state, which is bad enough, and would then be re-started when going
     * back to START, which could cause an application to stay in a restart
     * loop forever without watchdog timeout.
     *
     * In order to handle this, we will make sure to not actually delete an
     * existing registration file, but instead replace it with the new
     * file. */
    snprintf(path, PATH_MAX, "/proc/self/fd/%d", tmpfd);
    /* First we create a temporary directory entry for our new file, trying to
     * take care of concurrent calls to watchdog_register().  Although
     * concurrent watchdog_register() is not allowed, we want it to fail in a
     * known way.  If we were creating this temporary file without the use of
     * an unnamed temporary file first, we could end up renaming a partial
     * file, causing strange errors. */
    if (unlinkat(dirfd, "registration~", 0) && errno != ENOENT) {
        perror("unlinkat");
        fprintf(stderr, "%s: Error deleting registration~ file\n", __func__);
        goto out;
    }
    if (linkat(AT_FDCWD, path, dirfd, "registration~", AT_SYMLINK_FOLLOW)) {
        perror("linkat");
        fprintf(stderr, "%s: Error creating registration~ file\n", __func__);
        goto out;
    }
    /* Finally, we rename it to the real registration file.  This will now be
     * picked up by inotify(7) as an IN_MOVED_TO event. */
    if (renameat(dirfd, "registration~", dirfd, "registration")) {
        perror("renameat");
        fprintf(stderr, "%s: Error renaming registration~ to registration\n",
                __func__);
        goto out;
    }

    /* Cleanup client state files, so a restart of watchdog will start in
     * START state */
    if (unlinkat(dirfd, "ping", 0) && errno != ENOENT) {
        perror("unlinkat");
        fprintf(stderr, "%s: Could not remove ping file\n", __func__);
        goto out;
    }
    if (unlinkat(dirfd, "recover", 0) && errno != ENOENT) {
        perror("unlinkat");
        fprintf(stderr, "%s: Could not remove recover file\n", __func__);
        goto out;
    }

    retval = 0;

out:
    if (tmpfd != -1)
        close(tmpfd);
    if (dirfd != -1)
        close(dirfd);
    return retval;
}

int watchdog_open(const char *name)
{
    int dirfd = -1, pingfd = -1;

    /* Get handle to client interface directory */
    dirfd = client_dir(name);
    if (dirfd == -1) {
        fprintf(stderr, "%s: Could not open directory\n", __func__);
        return -1;
    }

    pingfd = openat(dirfd, "ping", O_RDWR|O_CREAT, 0644);
    if (pingfd == -1) {
        perror("openat");
        fprintf(stderr, "%s: Failed to open ping file\n", __func__);
        /* Note, the pingfd (-1) value will be returned */
    }

    close(dirfd);
    return pingfd;
}

int watchdog_keepalive(int pingfd)
{
    if (futimens(pingfd, NULL)) {
        perror("futimens");
        fprintf(stderr, "%s: Could not update timestamp on ping file\n",
                __func__);
        return -1;
    }
    return 0;
}

int watchdog_unregister(const char *name)
{
    int dirfd, retval = 0;

    /* Get handle to client interface directory */
    dirfd = client_dir(name);
    if (dirfd == -1) {
        fprintf(stderr, "%s: Could not open directory\n", __func__);
        return -1;
    }

    /* Cleanup ping file (in ALIVE or RECOVER state) */
    if (unlinkat(dirfd, "ping", 0) && errno != ENOENT) {
        perror("unlinkat");
        fprintf(stderr, "%s: Could not remove ping file\n", __func__);
        retval = -1;
    }

    /* Cleanup registration file (in ALIVE or RECOVER state) */
    if (unlinkat(dirfd, "registration", 0) && errno != ENOENT) {
        perror("unlinkat");
        fprintf(stderr, "%s: Could not remove registration file\n", __func__);
        retval = -1;
    }

    return retval;
}
