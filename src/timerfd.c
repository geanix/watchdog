/**
 * @file
 * Helper functions for timerfd handling.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

/**
 * Read number of expirations from timerfd file.
 *
 * On success, the number of times that the timer has expired since last call
 * is written to @p expirations.
 *
 * @param fd File descriptor to timerfd object.
 * @param expirations Output parameter.
 * @returns 0 on success, -1 on error.
 */
int timerfd_read_expirations(int fd, uint64_t *expirations)
{
    int retval = 0;
    retval = read(fd, expirations, sizeof(*expirations));
    if (retval == -1) {
        perror("read");
    } else if (retval != sizeof(*expirations)) {
        fprintf(stderr, "read: Unexpected length %d (expecting %zd)\n",
                retval, sizeof(*expirations));
        retval = -1;
    }
    if (retval == -1)
        fprintf(stderr, "%s: Error reading timer expirations\n", __func__);
    else
        retval = 0;
    return retval;
}
