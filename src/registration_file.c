/**
 * @file
 * Helper functions for handling client registration files.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "watchdog_int.h"

/**
 * Parse client registration file.
 *
 * This function parses the registration file in client directory.
 *
 * All arguments (except @p client_name) are output pointers.  On success,
 * parsed or default values are written to them.
 *
 * @param client_name Name of watchdog client (subdirectory of interface
 *   directory).
 *
 * @param start_timeout Timeout in milliseconds in START state.
 * @param keepalive_timeout Timeout in milliseconds in ALIVE state.
 * @param recover_cmd Shell command to run on entry to RECOVER state.
 * @param recover_timeout Timeout in milliseconds in RECOVER state.
 * @param recover_uid UID to use when running @p recover_cmd.
 *
 * @returns 0 on success, -1 on error.
 */
int registration_file_parse(const char *client_name,
                            unsigned long *start_timeout,
                            unsigned long *keepalive_timeout,
                            const char **recover_cmd,
                            unsigned long *recover_timeout,
                            uid_t *recover_uid)
{
    int retval = -1;
    const char *path;
    FILE *file;
    char *buf = 0, *delim, *key, *value;
    size_t buf_len = 0;
    ssize_t line_len;

    /* Get absolute path to registration file */
    path = client_file_path(client_name, "registration");
    if (path == NULL) {
        fprintf(stderr, "%s: Could not get path to registration file\n",
                __func__);
        return -1;
    }

    /* Open file for reading (O_RDONLY|O_CLOEXEC) and associate a stream with
     * it */
    file = fopen(path, "re");
    if (file == NULL) {
        fprintf(stderr, "%s: Opening registration file failed\n", __func__);
        return -1;
    }

    /* Pre-allocate a line buffer, so we most likely avoid realloc(3) calls by
     * getline(3) */
    buf_len = 4096;
    buf = malloc(buf_len);
    if (buf == NULL)
        out_of_memory();

    /* Apply default values first */
    *start_timeout = DEFAULT_START_TIMEOUT;
    *keepalive_timeout = DEFAULT_KEEPALIVE_TIMEOUT;
    *recover_cmd = NULL;
    *recover_timeout = 0;
    *recover_uid = -1;

    /* This loop calls getline(3) until EOF or error, thus reading the file
     * one line at a time.  Lines should be in key=value format.  Blank lines
     * are silently ignored. */
    while (1) {
        errno = 0;              /* Clear errno for reliable EOF detection */
        line_len = getline(&buf, &buf_len, file);
        if (line_len == -1) {
            if (errno == 0)     /* EOF */
                break;
            perror("getline");
            fprintf(stderr, "%s: Error while reading registration file\n",
                    __func__);
            goto out;
        }

        /* Strip trailing new-line */
        if (buf[line_len - 1] == '\n') {
            buf[line_len - 1] = '\0';
            line_len--;
        }

        if (line_len == 0)
            continue;           /* Skip empty lines */

        delim = strchr(buf, '=');
        if (delim == NULL)
            continue;           /* Skip lines without '=' delimiter */

        /* Split buf into key and value */
        *delim = '\0';
        key = buf;
        value = delim + 1;

        if (cstrncmp(key, "start_timeout") == 0) {
            if (parse_timeout_arg(start_timeout, value)) {
                fprintf(stderr, "%s: Error parsing start_timeout value: %s\n",
                        __func__, value);
                goto out;
            }
        } else if (cstrncmp(key, "keepalive_timeout") == 0) {
            if (parse_timeout_arg(keepalive_timeout, value)) {
                fprintf(stderr, "%s: Error parsing keepalive_timeout value\n",
                        __func__);
                goto out;
            }
        } else if (cstrncmp(key, "recover_timeout") == 0) {
            if (parse_timeout_arg(recover_timeout, value)) {
                fprintf(stderr, "%s: Error parsing recover_timeout value\n",
                        __func__);
                goto out;
            }
        } else if (cstrncmp(key, "recover_cmd") == 0) {
            *recover_cmd = strdup(value);
            if (*recover_cmd == NULL)
                out_of_memory();
        } else {
            fprintf(stderr, "%s: Invalid registration parameter: %s\n",
                    __func__, key);
            goto out;
        }
    }

    if (*recover_cmd != NULL) {
        /* Get UID for recover command by reading the UID from the
         * registration file */
        struct stat sb;
        if (stat(path, &sb)) {
            perror("stat");
            goto out;
        }
        *recover_uid = sb.st_uid;
    }

    retval = 0;

out:
    fclose(file);
    if (retval != 0 && *recover_cmd != NULL) {
        /* Avoid memory leak of recover_cmd string on error */
        free((void *)*recover_cmd);
        *recover_cmd = NULL;
    }
    return retval;
}
