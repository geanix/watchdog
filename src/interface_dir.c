/**
 * @file
 * Helper functions for handling the interface directory.
 *
 * Used in watchdog server command.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/inotify.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "watchdog_int.h"

/*
 * Helper functions for using inotify to listen for filesystem events.
 */

/** File descriptor to inotify(7) instance. */
static int inotify_fd;
/** Inotify watch descriptor for interface directory events. */
static int interface_watch;

/** Size of ::events_buf. */
#define EVENTS_BUF_SIZE 4096
/**
 * Buffer for reading inotify events into.
 *
 * Buffer must be at least sizeof(struct inotify_event) + NAME_MAX + 1 to
 * ensure it can contain one event.  But with NAME_MAX being 255, we can aford
 * to allocate more room, so we can hope to use only one read(2) call per for
 * each interface_dir_handle_events() call.
 */
static char events_buf[EVENTS_BUF_SIZE];

/**
 * Initialize inotify instance and watch interface directory.
 * @returns File descriptor to inotify instance on success, -1 on error.
 */
int interface_dir_init(void)
{
    /* Initialize inotify instance, setting it to non-blocking mode, so we can
     * simply call read(2) until EWOULDBLOCK */
    inotify_fd = inotify_init1(IN_NONBLOCK|IN_CLOEXEC);
    if (inotify_fd == -1) {
        perror("inotify_init1");
        return -1;
    }

    /* Add watch for interface directory */
    interface_watch = inotify_add_watch(inotify_fd, interface_dir,
                                        IN_CREATE|IN_DELETE);
    if (interface_watch == -1) {
        fprintf(stderr, "inotify_add_watch: %s: %s\n", interface_dir,
                strerror(errno));
        close(inotify_fd);
        return -1;
    }

    /* Return the file descriptor to poll on */
    return inotify_fd;
}

/**
 * Scan interface directory for already existing watchdog clients.
 * @returns 0 on success, -1 on error.
 */
int interface_dir_scan(void)
{
    DIR *dirp;
    struct dirent *dirent;

    dirp = opendir(interface_dir);
    if (dirp == NULL) {
        perror("opendir");
        return -1;
    }

    /* Loop over all directory entries in directory. The errno  */
    for (errno = 0, dirent = readdir(dirp);
         dirent != NULL;
         errno = 0, dirent = readdir(dirp)) {

        /* Directories */
        if (dirent->d_type == DT_DIR) {
            /* Ignore hidden directories */
            if (dirent->d_name[0] == '.')
                continue;
            /* Create new watchdog client state machine */
            client_fsm_new(dirent->d_name);
        }

        /* We don't care about anything else than directories */
    }

    if (errno != 0) {
        perror("readdir");
        closedir(dirp);
        return -1;
    }

    return 0;
}

/**
 * Get path to client directory.
 *
 * Construct absolute path to client directory, checking for errors and
 * truncation.
 *
 * Subsequent calls to client_dir_path() will invalidate the string from
 * previous call.
 *
 * @param name Name of client (i.e. subdirectory name).
 *
 * @returns Pointer to statically allocated string containing absolute path to
 *   client directory.
 */
static const char * client_dir_path(const char *name)
{
    static char path[PATH_MAX];
    int path_len;
    path_len = snprintf(path, sizeof(path), "%s/%s", interface_dir, name);
    if (path_len < 0) {
        fprintf(stderr, "%s: snprintf failed\n", __func__);
        return NULL;
    } else if (path_len >= sizeof(path)) {
        fprintf(stderr, "%s: snprintf was truncated\n", __func__);
        return NULL;
    }
    return path;
}

/**
 * Add a client directory watch to our inotify instance.
 *
 * @param name Name of client (i.e. subdirectory name).
 * @returns Inotify watch descriptor on success, -1 on error.
 */
int interface_dir_add_client_watch(const char *name)
{
    const char *client_dir;
    int client_watch;

    /* Get path to client directory */
    client_dir = client_dir_path(name);
    if (client_dir == NULL) {
        fprintf(stderr, "%s: client_dir_path failed\n", __func__);
        return -1;
    }

    /* Add watch for client directory, monitoring creation, deletion,
     * renaming, writing (or rather, the close of a writable file) and
     * metadata changes */
    client_watch = inotify_add_watch(
        inotify_fd, client_dir,
        IN_CREATE|IN_MOVED_TO|IN_DELETE|IN_ATTRIB);
    if (client_watch == -1) {
        fprintf(stderr, "inotify_add_watch: %s: %s\n", interface_dir,
                strerror(errno));
        return -1;
    }

    /* Return watch descriptor */
    return client_watch;
}

/**
 * Remove watch from the inotify instance.
 *
 * Remove a watch from the static inotify instance (::inotify_fd).
 *
 * @param wd Watch descriptor of watch to remove.
 * @returns 0 on success, -1 on error.
 */
int interface_dir_rm_watch(int wd)
{
    /* Remove watch */
    if (inotify_rm_watch(inotify_fd, wd)) {
        perror("inotify_add_watch");
        fprintf(stderr, "%s: Error removing watch\n", __func__);
        return -1;
    }

    return 0;
}

/**
 * Handle interface directory inotify event.
 *
 * This function is called with inotify events originating from
 * ::interface_watch.
 *
 * @param event Inotify event received.
 */
static void handle_interface_dir_event(struct inotify_event *event)
{
    if ((event->mask & (IN_CREATE|IN_ISDIR)) == (IN_CREATE|IN_ISDIR)) {
        /* New directory created in interface directory, create new client
         * state machine */
        client_fsm_new(event->name);
        return;
    } else if ((event->mask & (IN_DELETE|IN_ISDIR)) == (IN_DELETE|IN_ISDIR)) {
        /* Directory in interface directory deleted, delete client state
         * machine */
        client_fsm_destroy(event->name);
        return;
    }
    /* Ignoring all other events */
}

/**
 * Handle client directory inotify event.
 *
 * This function is called with inotify events originating from client
 * directory watches, and translates the event into more abstract client
 * events, which is then fed to the client state machine.
 *
 * @param event Inotify event received.
 * @sa client_fsm_watch_event().
 */
static void handle_client_dir_event(struct inotify_event *event)
{
    if (event->name && strcmp(event->name, "registration") == 0) {
        if (event->mask & (IN_CREATE|IN_MOVED_TO))
            /* The registration file was created, i.e. watchdog_register() was
             * called */
            client_fsm_watch_event(event->wd, CLIENT_REGISTER);
        else if (event->mask & IN_DELETE)
            /* The registration file was deleted, i.e. watchdog_unregister()
             * was called */
            client_fsm_watch_event(event->wd, CLIENT_UNREGISTER);
    } else if (event->name && strcmp(event->name, "ping") == 0) {
        if (event->mask & IN_CREATE)
            /* The ping file was created, i.e. watchdog_open() was called */
            client_fsm_watch_event(event->wd, CLIENT_OPEN);
        else if (event->mask & IN_ATTRIB)
            /* A meta-data change of ping file, assuming watchdog_keepalive()
             * was called */
            client_fsm_watch_event(event->wd, CLIENT_KEEPALIVE);
    }
    /* We don't care about events from other files. */
}

/**
 * Handle a single inotify event.
 *
 * @param event Inotify event received.
 * @returns Next event in ::events_buf.
 * @sa interface_dir_handle_events(), handle_interface_dir_event(),
 *   handle_client_dir_event().
 */
static struct inotify_event *handle_event(struct inotify_event *event)
{
    if (event->wd == interface_watch)
        handle_interface_dir_event(event);
    else
        handle_client_dir_event(event);

    /* Advance pointer to next event.  The size of the current event is
     * sizeof(struct inotify_event) + event->len.  If name is padded, it will
     * be included in the len field. */
    if (event->len)
        event = (struct inotify_event *)((char *)event + event->len);
    event += 1;
    return event;
}

/**
 * Handle all queued inotify events.
 *
 * This events reads and handles inotify events until there are no more events.
 *
 * @sa handle_event().
 */
void interface_dir_handle_events(void)
{
    ssize_t buflen;
    struct inotify_event *event;

    /* Loop over read(2) from inotify instance, reading out all available
     * events */
    while (1) {
        buflen = read(inotify_fd, events_buf, sizeof(events_buf));
        if (buflen == -1) {
            /* Read failed */
            if (errno == EAGAIN)
                /* Good, no more events for now.  We are done. */
                return;
            /* Hmm...  Reading from inotify instance should not fail, and it
             * is probably best to exit immediately, hoping that we can
             * quickly restart and recover before it gets really bad. */
            perror("read");
            fprintf(stderr, "%s: Failed to read from inotify instance\n",
                    __func__);
            fputs("Aborting\n", stderr);
            exit(EXIT_FAILURE);
        }

        /* Now we should have one or more events in the buffer.  Notice, there
         * will never be partial events in the buffer, so we do not have to
         * try and handle that. */
        event = (struct inotify_event *)events_buf;
        /* Loop over each event */
        while ((void *)event < (void *)(events_buf + buflen))
            /* Handle this event and get next event back */
            event = handle_event(event);
    }
}
