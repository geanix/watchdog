/**
 * @file
 * Stringification macros.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/**
 * 2nd level stringify macro.
 *
 * Stringifies @p s, with macro-expansion.
 *
 * @code
 * #define xstr(s) str(s)
 * #define str(s) #s
 * #define foo 4
 * xstr(foo)
 *     ==> xstr(4)
 *     ==> str(4)
 *     ==> "4"
 * @endcode
 *
 * @param s String constant.
 */
#define xstr(s) str(s)
/**
 * 1st level stringify macro.
 *
 * Stringifies @p s, but does not macro-expand it first.
 *
 * @code
 * #define xstr(s) str(s)
 * #define str(s) #s
 * #define foo 4
 * str(foo)
 *     ==> "foo"
 * @endcode
 *
 * @param s String constant.
 * @returns Stringified @p s.  */
#define str(s) #s
