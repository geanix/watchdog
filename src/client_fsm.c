/**
 * @file
 * Implementation of state machine for watchdog clients.
 *
 * A state machine instance will be created for each directory in
 * /run/watchdog.
 *
 * API actions (libwatchdog) will be received with inotify(7) (see
 * interface_dir.c), and passed as events to the corresponding state machine
 * instance.  Timeouts (from START, ALIVE and RECOVER states) is implemented
 * with timerfd and are also converted to the corresponding state machine (see
 * interface_dir.c).
 *
 * The state is fully represented by a simple enum type (@ref client_state_t).
 *
 * The state transition table is implemented using a simple two dimensional
 * switch-case struct (see dispatch_event()), with actions being defined as
 * exit and entry actions in state_exit_actions() and state_entry_actions()
 * correspondingly.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/timerfd.h>
#include <sys/epoll.h>
#include <sys/types.h>

#include "watchdog_int.h"
#include "list.h"

/** Client states. */
enum client_state {
    CLIENT_READY = 0,
    CLIENT_START,
    CLIENT_ALIVE,
    CLIENT_RECOVER,
    CLIENT_DEAD,
};
/** Client states. */
typedef enum client_state client_state_t;

/**
 * Watchdog client state machine.
 *
 * At any point in time, a client state machine is in exactly one discrete
 * state, as represented by client_fsm.state.
 *
 * Transitions are implemented by dispatch_event() which changes the state
 * based on the current state and an event (::client_event).  Each state has
 * a specific entry and exit actions as defined by state_entry_actions() and
 * state_exit_actions() respectively.
 */
struct client_fsm {
    /** Linked list header (::clients). */
    struct list_head list;
    /** Client name. */
    const char *name;
    /** Current state. */
    client_state_t state;
    /** Inotify watch descriptor. */
    int wd;
    /** Timerfd file descriptor. */
    int timer;
    /** Timeout in milliseconds in CLIENT_START (::client_state). */
    unsigned long start_timeout;
    /** Timeout in milliseconds in CLIENT_ALIVE (::client_state). */
    unsigned long keepalive_timeout;
    /** Shell command to run (fork+exec) on entry to CLIENT_RECOVER
     * (::client_state). */
    const char *recover_cmd;
    /** Timeout in milliseconds in CLIENT_RECOVER (::client_state). */
    unsigned long recover_timeout;
    /** UID to use when running client_fsm.recover_cmd */
    uid_t recover_uid;
    /** PID of running client_fsm.recover_cmd */
    pid_t recover_pid;
};
/** Watchdog client state machine. */
typedef struct client_fsm client_fsm_t;

/**
 * @var clients
 * Linked list of client state machines (::client_fsm).
 */
struct list_head clients = LIST_HEAD_INIT(clients);

/**
 * Get string describing client event.
 * @param event Event to describe.
 * @returns Pointer to statically allocated string describing @p event.
 */
static const char * event_name(client_event_t event)
{
    switch (event) {
    case CLIENT_REGISTER:
        return "register";
    case CLIENT_UNREGISTER:
        return "unregister";
    case CLIENT_OPEN:
        return "open";
    case CLIENT_KEEPALIVE:
        return "keepalive";
    case CLIENT_TIMEOUT:
        return "timeout";
    default:
        return "<unknown>";
    }
}

/**
 * Get string describing client state.
 * @param state State to describe.
 * @returns Pointer to statically allocated string describing @p state.
 */
static const char * state_name(client_state_t state)
{
    switch (state) {
    case CLIENT_READY:
        return "READY";
    case CLIENT_START:
        return "START";
    case CLIENT_ALIVE:
        return "ALIVE";
    case CLIENT_RECOVER:
        return "RECOVER";
    case CLIENT_DEAD:
        return "DEAD";
    default:
        return "<unknown>";
    }
}

/**
 * Lookup client state machine by name.
 *
 * @param name Name of client state machine.
 * @returns Pointer to state machine with client_fsm.name == @p name, or NULL
 *   if no matching state machine is found.
 */
static client_fsm_t * get_client_fsm_from_name(const char *name)
{
    struct list_head *entry;
    client_fsm_t *fsm;

    /* Loop over all client state machines */
    list_for_each(entry, &clients) {
        fsm = container_of(entry, client_fsm_t, list);
        if (strcmp(fsm->name, name) == 0)
            /* Return matching client state machine */
            return fsm;
    }

    /* No matching client state machine found */
    return NULL;
}

/**
 * Lookup client state machine by inotify watch descriptor.
 *
 * @param wd Inotify watch descriptor of client state machine.
 * @returns Pointer to state machine with client_fsm.wd == @p wd, or NULL if
 *   no matching state machine is found.
 */
static client_fsm_t * get_client_fsm_from_watch(int wd)
{
    struct list_head *entry;
    client_fsm_t *fsm;

    /* Loop over all client state machines */
    list_for_each(entry, &clients) {
        fsm = container_of(entry, client_fsm_t, list);
        if (fsm->wd == wd)
            /* Return matching client state machine */
            return fsm;
    }

    /* No matching client state machine found */
    return NULL;
}

/**
 * Lookup client state machine by inotify watch descriptor.
 *
 * @param fd Timerfd file descriptior of client state machine.
 * @returns Pointer to state machine with client_fsm.timer == @p fd, or NULL
 *   if no matching state machine is found.
 */
static client_fsm_t * get_client_fsm_from_timer(int fd)
{
    struct list_head *entry;
    client_fsm_t *fsm;

    /* Loop over all client state machines */
    list_for_each(entry, &clients) {
        fsm = container_of(entry, client_fsm_t, list);
        if (fsm->timer == fd)
            /* Return matching client state machine */
            return fsm;
    }

    /* No matching client state machine found */
    return NULL;
}

/**
 * Lookup client state machine name by PID of recover command.
 *
 * @param pid PID of recover command.
 * @returns Pointer to state machine with client_fsm.recover_pid == @p pid, or
 *   NULL if no matching state machine is found.
 */
const char * client_fsm_name_from_recover_pid(pid_t pid)
{
    struct list_head *entry;
    client_fsm_t *fsm;

    /* Loop over all client state machines */
    list_for_each(entry, &clients) {
        fsm = container_of(entry, client_fsm_t, list);
        if (fsm->recover_pid == pid)
            /* Return matching client state machine name */
            return fsm->name;
    }

    /* No matching client state machine found */
    return NULL;
}

/**
 * Start client timer.
 *
 * This arms the client timer (client_fsm.timer), firing a timer event
 * (timeout) in @p ms milliseconds.
 *
 * @param fsm Client state machine.
 * @param ms Timeout in milliseconds.
 * @returns 0 on success, -1 on error.
 */
static int timer_set(client_fsm_t *fsm, unsigned long ms)
{
    struct itimerspec timer_spec = {0};
    timer_spec.it_value.tv_sec = ms / 1000;
    timer_spec.it_value.tv_nsec = (ms % 1000) * 1000000;
    if (timerfd_settime(fsm->timer, 0, &timer_spec, NULL)) {
        perror("timerfd_settime");
        fprintf(stderr, "%s: Failed to arm client timer\n", __func__);
        return -1;
    }
    return 0;
}

/**
 * Clear client timeout.
 *
 * This disarms the client timer (client_fsm.timer), and clears any already
 * fired timer events.
 *
 * @param fsm Client state machine.
 * @returns 0 on success, -1 on error.
 */
static int timer_clear(client_fsm_t *fsm)
{
    struct itimerspec timer_spec = {0};
    if (timerfd_settime(fsm->timer, 0, &timer_spec, NULL)) {
        perror("timerfd_settime");
        fprintf(stderr, "%s: Failed to disarm client timer\n", __func__);
        return -1;
    }
    return 0;
}

/**
 * Actions to execute on exit from a state.
 *
 * This function is called on exit from a client state, implementing the
 * actions needed on state exit.
 *
 * @param fsm Client state machine.
 */
static void state_exit_actions(client_fsm_t *fsm)
{
    switch (fsm->state) {
    case CLIENT_READY:
        /* Nothing to do */
        break;
    case CLIENT_START:
        /* Clear timer */
        timer_clear(fsm);
        break;
    case CLIENT_ALIVE:
        /* Clear timer */
        timer_clear(fsm);
        break;
    case CLIENT_RECOVER:
        /* Clear timer */
        timer_clear(fsm);
        break;
    case CLIENT_DEAD:
        /* Will never happen */
        break;
    }
}


/**
 * Actions to execute on entry to a state.
 *
 * This function is called on entry to a client state, implementing the
 * actions needed on state entry.
 *
 * @param fsm Client state machine.
 */
static void state_entry_actions(client_fsm_t *fsm)
{
    switch (fsm->state) {
    case CLIENT_READY:
        /* Nothing to do */
        break;
    case CLIENT_START:
    {
        /* Parse registration file and start timer */
        unsigned long old_start_timeout = fsm->start_timeout;
        if (registration_file_parse(
                fsm->name,
                &fsm->start_timeout, &fsm->keepalive_timeout,
                &fsm->recover_cmd, &fsm->recover_timeout, &fsm->recover_uid)) {
            fprintf(stderr, "%s: Parsing registration file failed\n",
                    fsm->name);
            /* This should not happen as registration file should be written
             * by watchdog_register() API function, and should always be
             * valid.  But if it happens anyway, we cannot really fix it, and
             * we cannot report the error to whatever wrote the bad file.  And
             * ignoring the watchdog client entirely is not necessarily better
             * than using default values.  So we go with the latter option. */
        }
        /* Reset timer if the start_timeout value has changed, avoiding
         * infinite loop if client dies in START, while still respecting a
         * restart to a new application using a different start_timeout
         * value */
        if (fsm->start_timeout != old_start_timeout)
            timer_set(fsm, fsm->start_timeout);
        break;
    }
    case CLIENT_ALIVE:
        /* Start timer */
        timer_set(fsm, fsm->keepalive_timeout);
        break;
    case CLIENT_RECOVER:
        /* Write state file and start timer */
        client_file_write(fsm->name, "recover");
        timer_set(fsm, fsm->recover_timeout);
        fprintf(stderr, "%s: Running recover command (uid %u): %s\n",
                fsm->name, fsm->recover_uid, fsm->recover_cmd);
        fsm->recover_pid = recover_cmd_fork(fsm->recover_cmd, fsm->recover_uid);
        break;
    case CLIENT_DEAD:
        /* Write state file */
        client_file_write(fsm->name, "R.I.P.");
        monitor_client_died();
        break;
    }
}

/**
 * Detect the client state on server startup.
 *
 * Check the client directory content, determining the state based on that.
 * See table in design documentation for an overview.
 *
 * @param name Name of client state machine.
 * @returns Client state.
 */
static client_state_t detect_initial_state(const char *name)
{
    /* Note: client_file_exists() can fail for various reasons, mainly errors
     * coming from stat(2).  The checks here tries to make safe choice in case
     * of error, basically assuming that the file does not exist in case of
     * error. */
    if (client_file_exists(name, "R.I.P.") == 1)
        return CLIENT_DEAD;
    else if (client_file_exists(name, "recover") == 1)
        return CLIENT_RECOVER;
    else if (!(client_file_exists(name, "registration") == 1))
        return CLIENT_READY;
    else if (client_file_exists(name, "ping") == 1)
        return CLIENT_ALIVE;
    else
        return CLIENT_START;
}

/**
 * Create new client state machine.
 *
 * This function creates a new client state machine from a client directory.
 * This is called on startup and when a new client directory is created.
 *
 * When a client shuts down, the client state machine is kept (in CLIENT_READY
 * state).  The state machine is only destroyed if the client directory is
 * removed (see client_fsm_destroy()).
 *
 * @param name Name of client state machine.
 */
void client_fsm_new(const char *name)
{
    client_fsm_t *fsm;

    /* Check if client state machine already exists */
    fsm = get_client_fsm_from_name(name);
    if (fsm != NULL)
        /* Client state machine already created.  Possible race condition.
         *
         * If a client directory is created concurrently with watchdog server
         * startup, after we have added inotify watch, but before we have
         * scanned interface directory, we can get the IN_CREATE event for an
         * already created client state machine.  The event can be safely
         * ignored. */
        return;

    /* Allocate new client state machine */
    fsm = calloc(1, sizeof(*fsm));
    if (fsm == NULL)
        out_of_memory();
    fsm->name = strdup(name);
    if (fsm->name == NULL)
        out_of_memory();

    /* Create timer object for use by the various timeout events */
    fsm->timer = timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC|TFD_NONBLOCK);
    if (fsm->timer == -1) {
        perror("timerfd_create");
        goto err_timerfd_create;
    }
    /* Add it to event_loop epoll file descriptor */
    if (event_loop_add_fd(fsm->timer, EPOLLIN)) {
        fprintf(stderr, "%s: Error adding timer to epoll\n", __func__);
        goto err_timer_add;
    }

    /* Start watching client directory for filesystem events */
    fsm->wd = interface_dir_add_client_watch(name);
    if (fsm->wd == -1) {
        fprintf(stderr, "%s: Error adding client directory watch\n", __func__);
        goto err_add_watch;
    }

    /* Scan client directory for initial state */
    fsm->state = detect_initial_state(name);
    fprintf(stderr, "%s: Initial state: %s\n",
            fsm->name, state_name(fsm->state));
    /* Normally, registration file is parsed on entry to START, so remember to
     * do that when skipping ahead of START */
    switch (fsm->state) {
    case CLIENT_ALIVE:
    case CLIENT_RECOVER:
    case CLIENT_DEAD:
        if (registration_file_parse(
                fsm->name,
                &fsm->start_timeout, &fsm->keepalive_timeout,
                &fsm->recover_cmd, &fsm->recover_timeout, &fsm->recover_uid)) {
            fprintf(stderr, "%s: Parsing registration file failed\n",
                    fsm->name);
        }
        break;
    default:
        break;
    }

    /* Execute entry actions */
    state_entry_actions(fsm);

    /* Add to list of client state machines */
    list_add(&fsm->list, &clients);

    return;

err_add_watch:
    event_loop_rm_fd(fsm->timer);
err_timer_add:
    close(fsm->timer);
err_timerfd_create:
    free((void *)fsm->name);
    free((void *)fsm);
    fprintf(stderr, "%s: Creating client state machine failed\n", name);
    return;

}

/**
 * Destroy client state machine.
 *
 * This functions removes a client state machine and frees resources attached
 * to it.
 *
 * This is called when client directory is removed.
 *
 * @param name Name of client state machine.
 */
void client_fsm_destroy(const char *name)
{
    client_fsm_t *fsm;

    /* Check if client state machine already exists */
    fsm = get_client_fsm_from_name(name);
    if (fsm == NULL)
        /* Client state machine not found.  Possible race condition.
         *
         * If a client directory is deleted concurrently with watchdog server
         * startup, after we have added inotify watch, but before we have
         * scanned interface directory, we can get the IN_DELETE event for a
         * non-existing client state machine.  The event can be safely
         * ignored. */
        return;

    fprintf(stderr, "%s: Deleting client state machine\n", name);

    /* Delete from list of client state machines */
    list_del(&fsm->list);

    /* Close/destroy timer object */
    close(fsm->timer);

    /* Remove client directory watch from inotify instance */
    interface_dir_rm_watch(fsm->wd);

    /* Free allocated memory */
    free((void *)fsm->name);
    free(fsm);
}

/**
 * The client state machine event dispatcher function.
 *
 * This takes a client state machine and an event, and normally results in a
 * transition with some attached actions.
 *
 * With a small number of states and events, this is implemented in classic
 * switch-case style.
 *
 * @param fsm Client state machine.
 * @param event Event to dispatch.
 * @sa state_entry_actions(), state_exit_actions().
 */
void dispatch_event(client_fsm_t *fsm, client_event_t event)
{
    client_state_t transition_to = -1;
    bool invalid_event = false;      /* event is not valid in current state */
    bool ignore_event = false;       /* just ignore this event */
    bool skip_exit_actions = false;  /* skip exit actions for transition */
    bool skip_entry_actions = false; /* skip entry actions for transition */
    bool log_transition = true;      /* write transition log message */

    switch (fsm->state) {
        /* 1st level state transition switch-case: current state */

    case CLIENT_READY:
        switch (event) {
            /* 2nd level state transition switch-case: dispatched event */
        case CLIENT_REGISTER:
            transition_to = CLIENT_START;
            break;
        case CLIENT_OPEN:
        case CLIENT_KEEPALIVE:
            /* It should be safe to ignore early open and keepalive events.
             * They should not come from correct use of the API.  When the
             * watchdog_open() call is made, an event should arrive in correct
             * order. */
            ignore_event = true;
            break;
        default:
            invalid_event = true;
            break;
        }
        break;

    case CLIENT_START:
        switch (event) {
            /* 2nd level state transition switch-case: dispatched event */
        case CLIENT_REGISTER:
            /* A very likely race conditions (as in most of the time!), the
             * registration file will already be created before we scan the
             * client directory, so we need to ignore this event.  We have
             * made sure that it is atomically created, so it should always be
             * parsed in its entirety.
             *
             * Another case for receiving register event in START state is a
             * client restart during START, in which case we need to parse the
             * registration file again, and possibly restart the timer.
             *
             * By taking the (specially crafted) entry actions to START, we
             * handle both above cases.  The entry actions always reparse the
             * file, but only restarts the timer if start_timeout has changed
             * since last parse. */
            transition_to = CLIENT_START;
            skip_exit_actions = true;
            break;
        case CLIENT_KEEPALIVE:
            /* In case of non-API creation of ping file, the call to
             * watchdog_open() could be received as an alive event, so we will
             * handle it in the same way as open. */
            /* Intentional fall through to CLIENT_OPEN ... */
        case CLIENT_OPEN:
            transition_to = CLIENT_ALIVE;
            break;
        case CLIENT_UNREGISTER:
            transition_to = CLIENT_READY;
            break;
        case CLIENT_TIMEOUT:
            transition_to = CLIENT_DEAD;
            break;
        default:
            invalid_event = true;
            break;
        }
        break;

    case CLIENT_ALIVE:
        switch (event) {
            /* 2nd level state transition switch-case: dispatched event */
        case CLIENT_KEEPALIVE:
            transition_to = CLIENT_ALIVE;
            log_transition = false;
            break;
        case CLIENT_REGISTER:
            transition_to = CLIENT_START;
            break;
        case CLIENT_UNREGISTER:
            transition_to = CLIENT_READY;
            break;
        case CLIENT_TIMEOUT:
            if (fsm->recover_cmd)
                transition_to = CLIENT_RECOVER;
            else
                transition_to = CLIENT_DEAD;
            break;
        default:
            invalid_event = true;
            break;
        }
        break;

    case CLIENT_RECOVER:
        switch (event) {
            /* 2nd level state transition switch-case: dispatched event */
        case CLIENT_KEEPALIVE:
            /* Late keep-alive event */
            ignore_event = true;
            break;
        case CLIENT_REGISTER:
            transition_to = CLIENT_START;
            break;
        case CLIENT_UNREGISTER:
            transition_to = CLIENT_READY;
            break;
        case CLIENT_TIMEOUT:
            transition_to = CLIENT_DEAD;
            break;
        default:
            invalid_event = true;
            break;
        }
        break;

    case CLIENT_DEAD:
        switch (event) {
            /* 2nd level state transition switch-case: dispatched event */
        case CLIENT_REGISTER:
        case CLIENT_UNREGISTER:
        case CLIENT_OPEN:
        case CLIENT_KEEPALIVE:
            /* Ignore all client initiated events.  When declared dead, there
             * is no way out. */
            ignore_event = true;
            break;
        default:
            invalid_event = true;
            break;
        }
        break;

    }

    if (ignore_event) {
        return;
    } else if (invalid_event) {
        fprintf(stderr, "%s: Event %s not allowed in state %s\n",
                __func__, event_name(event), state_name(fsm->state));
        return;
    }

    if (log_transition)
        fprintf(stderr, "%s: Transition: %s + %s -> %s\n",
                fsm->name, state_name(fsm->state), event_name(event),
                state_name(transition_to));
    if (!skip_exit_actions)
        state_exit_actions(fsm);
    fsm->state = transition_to;
    if (!skip_entry_actions)
        state_entry_actions(fsm);
}

/**
 * Dispatch client event from inotify watch to client state machine.
 *
 * This function is called with a client event, and dispatches it to the
 * client state machine with the @p wd watch.
 *
 * @param wd Inotify watch descriptor of client state machine to dispatch
 *   event to.
 * @param event Event to dispatch.
 */
void client_fsm_watch_event(int wd, client_event_t event)
{
    client_fsm_t *fsm;

    /* Get the matching client state machine */
    fsm = get_client_fsm_from_watch(wd);
    if (fsm == NULL) {
        /* This should not happen, but in case it does, simply log an error
         * message and try to continue.  An overly eager watchdog does not
         * make many friends... */
        fprintf(stderr, "%s: Client state machine not found\n", __func__);
        return;
    }

    dispatch_event(fsm, event);
}

/**
 * Dispatch client timeout event from client timer.
 *
 * @param fd File descriptor of client state machine timer to dispatch event
 *   to.
 */
void client_fsm_timeout(int fd)
{
    client_fsm_t *fsm;
    uint64_t expirations = 0;

    /* Get client state machine */
    fsm = get_client_fsm_from_timer(fd);
    if (fsm == NULL) {
        /* This should not happen, but in case it does, simply log an error
         * message and try to continue.  An overly eager watchdog does not
         * make many friends... */
        fprintf(stderr, "%s: Could not find client state machine\n",__func__);
        return;
    }

    if (timerfd_read_expirations(fd, &expirations)) {
        fprintf(stderr, "%s: Error reading timerfd object\n", __func__);
    }

    dispatch_event(fsm, CLIENT_TIMEOUT);
}
