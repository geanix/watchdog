/**
 * @file
 * Internal header file for watchdog server command.
 *
 * Contains non-static function prototypes, global variable declarations, and
 * convenience macros for use exclusively in watchdog server command.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef WATCHDOG_INT_H
#define WATCHDOG_INT_H

#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <sys/types.h>

#include "arg_parse.h"

extern bool dry_run;
extern const char *interface_dir;

/* event_loop.c prototypes */
int event_loop(int dev_fd, int device_timer_fd, int interface_dir_fd);
int event_loop_add_fd(int fd, uint32_t events);
void event_loop_rm_fd(int fd);

/* timerfd.c prototypes */
int timerfd_read_expirations(int fd, uint64_t *expirations);

/* timespec.c prototypes */
void timespec_add(struct timespec *t1, struct timespec *t2);
bool timespec_before(struct timespec *t1, struct timespec *t2);

/* device.c prototypes */
int device_open(const char *path);
void device_close(int fd, bool magic);
bool device_supports_settimeout(int fd);
void device_settimeout(int fd, unsigned long timeout_ms);
unsigned long device_gettimeout(int fd);
void device_keepalive(int fd);
int device_timer(unsigned long interval);
void device_timer_event(int fd);

/* interface_dir.c prototypes */
int interface_dir_init(void);
int interface_dir_scan(void);
int interface_dir_add_client_watch(const char *name);
int interface_dir_rm_watch(int wd);
void interface_dir_handle_events(void);

/* registration_file.c prototypes */
int registration_file_parse(const char *client_name,
                            unsigned long *start_timeout,
                            unsigned long *keepalive_timeout,
                            const char **recover_cmd,
                            unsigned long *recover_timeout,
                            uid_t *recover_uid);

/* client_fsm.c prototypes */
/** Client state machine event */
enum client_event {
    CLIENT_REGISTER,
    CLIENT_UNREGISTER,
    CLIENT_OPEN,
    CLIENT_KEEPALIVE,
    CLIENT_TIMEOUT,
};
/** Client state machine event */
typedef enum client_event client_event_t;
void client_fsm_new(const char *name);
void client_fsm_destroy(const char *name);
void client_fsm_watch_event(int wd, client_event_t event);
void client_fsm_timeout(int fd);
const char * client_fsm_name_from_recover_pid(pid_t pid);

/* client_file.c prototypes */
const char * client_file_path(const char *client_name,
                              const char *file_name);
int client_file_write(const char *client_name, const char *file_name);
int client_file_exists(const char *client_name, const char *file_name);
int client_file_unlink(const char *client_name, const char *file_name);

/* monitor.c prototypes */
void monitor_init(unsigned long reboot_timeout_ms);
void monitor_client_died(void);
bool monitor_keepalive(void);

/* monitor_file.c prototypes */
int monitor_file_exists(const char *file_name);
int monitor_file_write(const char *file_name);

/* recover_cmd.c prototypes */
int recover_cmd_fork(const char *command, uid_t uid);
void recover_cmd_zombie_handler(int sig);

/* Convenience macros */
#include <stdio.h>
#include <stdlib.h>
/**
 * Out of memory handler.
 * Print error message and exit program with EXIT_FAILURE.
 */
#define out_of_memory() do {                    \
    fputs("Out of memory\n", stderr);           \
    exit(EXIT_FAILURE);                         \
} while (0)
/**
 * Convenience macro for using strncmp(3) on a constant string.
 * @param str String pointer to compare.
 * @param cstr String constant to compare with.
 * @returns 0 if @p str is equal to @p cstr, non-zero otherwise.
 */
#define cstrncmp(str, cstr) strncmp(str, cstr, strlen(cstr))

#endif /* WATCHDOG_INT_H */
