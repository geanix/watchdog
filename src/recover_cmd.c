/**
 * @file
 * Implementation of recover command.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "watchdog_int.h"

/**
 * Signal handler for reaping recover command zombies.
 *
 * This function is installed to SIGCHLD by install_zombie_handler(), and will
 * make sure that resources owned by recover command childs are freed when
 * they exit.
 *
 * Also prints out a message on child termination to stderr.
 *
 * @param sig Signal received (should be SIGCHLD).
 */
void recover_cmd_zombie_handler(int sig)
{
    siginfo_t info;
    const char *client_name;

    assert(sig == SIGCHLD);

    /* Loop over all exited child processes, effectively reaping them from
     * zombie state. */
    while (1) {
        /* Query for a child process that has exited */
        if (waitid(P_ALL, 0, &info, WEXITED|WNOHANG)) {
            if (errno == ECHILD)
                /* No child processes.  Strange, but I guess we are done. */
                return;

            perror("waitid");
            /* This is bad.  We cannot allow watchdog server to eat up system
             * resources by a horde of zombies.  Better get out now. */
            exit(EXIT_FAILURE);
        }
        if (info.si_pid == 0)
            /* No more child processes that has exited.  We are done. */
            return;

        /* Try to lookup client name */
        client_name = client_fsm_name_from_recover_pid(info.si_pid);

        switch (info.si_code) {
        case CLD_EXITED:
            if (client_name) {
                if (info.si_status == 0)
                    fprintf(stderr, "%s: Recover command completed\n",
                            client_name);
                else
                    fprintf(stderr, "%s: Recover command exited %u\n",
                            client_name, info.si_status);
            } else {
                fprintf(stderr, "Child (pid %u) exited %u\n",
                        info.si_pid, info.si_status);
            }
            break;
        case CLD_KILLED:
        case CLD_DUMPED:
            if (client_name)
                fprintf(stderr,
                        "%s: Recover command killed by signal %u (%s)\n",
                        client_name, info.si_status, strsignal(info.si_status));
            else
                fprintf(stderr,
                        "Child (pid %u) killed by signal %u (%s)\n",
                        info.si_pid, info.si_status, strsignal(info.si_status));
            break;
        default:
            /* We don't really care about CLD_STOPPED, CLD_TRAPPED, and
             * CLD_CONTINUED */
            break;
        }
    }
}

/**
 * Fork and exec recover command.
 *
 * A simple fork and exec wrapper, forking the current process, changing UID
 * and executing the specified shell command string.
 *
 * @param command The shell command string to execute.
 * @param uid UID to change to before executing @p command.
 * @returns PID of the child.
 */
pid_t recover_cmd_fork(const char *command, uid_t uid)
{
    pid_t pid;

    pid = fork();
    if (pid == -1) {
        perror("fork");
        return 0;
    }

    if (pid != 0)
        /* Parent process, return PID to watchdog server */
        return pid;

    /* Child process */

    /* Change UID if needed */
    if (getuid() != uid) {
        if (setuid(uid)) {
            perror("setuid");
            fprintf(stderr, "Recover command not executed\n");
            exit(EXIT_FAILURE);
        }
    }


    /* Replace current process image (watchdog server) with /bin/sh running
     * the specified recover command */
    execl("/bin/sh", "sh", "-c", command, NULL);

    /* Will never be reached */
    assert(false);
    return 0;
}
