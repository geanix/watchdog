/**
 * @file
 * Helper functions for parsing command-line arguments.
 *
 * Used in watchdog server command and and watchdog_register client command.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <limits.h>

/**
 * Parse timeout argument.
 *
 * Parse an integer number of seconds, or if suffixed with "ms", an integer
 * number of milliseconds.  The value is always returned as milliseconds.
 *
 * @param timeout On success, number of milliseconds is written to this
 *   pointer.
 * @param arg String argument to parse.
 * @returns 0 on success, -1 on error (and errno is set appropriately).
 */
int parse_timeout_arg(unsigned long *timeout, const char *arg)
{
    char *endptr;
    unsigned long retval;
    errno = 0;                  /* Set to zero for strtoul() error checking */
    retval = strtoul(arg, &endptr, 10);
    if ((errno == EINVAL) || (errno == ERANGE && retval == ULONG_MAX)) {
        int _errno = errno;     /* Protect errno for perror() disruption */
        perror("strtoul");
        errno = _errno;
        return -1;
    }
    if (*endptr == '\0') {
        *timeout = retval * 1000;
        return 0;
    }
    if (strcmp(endptr, "ms") == 0) {
        *timeout = retval;
        return 0;
    }
    errno = EINVAL;
    return -1;
}
