/**
 * @file
 * Helper functions for handling simple files in client directory.
 *
 * Client directories are subdirectories of ::interface_dir.
 *
 * Used in watchdog server command.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"

#include <stdio.h>
#include <errno.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "watchdog_int.h"

/**
 * Get path to file in client directory.
 *
 * Construct absolute path to client directory, checking for errors and
 * truncation.
 *
 * Subsequent calls to client_file_path() will invalidate the string from
 * previous call.
 *
 * @param client_name Name of client (i.e. subdirectory name).
 * @param file_name Name of client file.
 *
 * @returns Pointer to statically allocated string containing absolute path to
 *   client file.
 */
const char * client_file_path(const char *client_name,
                              const char *file_name)
{
    static char path[PATH_MAX];
    int path_len;
    path_len = snprintf(path, sizeof(path), "%s/%s/%s",
                        interface_dir, client_name, file_name);
    if (path_len < 0) {
        fprintf(stderr, "%s: snprintf failed\n", __func__);
        return NULL;
    } else if (path_len >= sizeof(path)) {
        fprintf(stderr, "%s: snprintf was truncated\n", __func__);
        return NULL;
    }
    return path;
}

/**
 * Write empty file in client directory.
 *
 * @param client_name Name of client (i.e. subdirectory name).
 * @param file_name Name of client file.
 *
 * @returns 0 on success, -1 on error.
 */
int client_file_write(const char *client_name, const char *file_name)
{
    const char *client_file;
    int fd;

    /* Get path to file */
    client_file = client_file_path(client_name, file_name);
    if (client_file == NULL) {
        fprintf(stderr, "%s: client_file_path failed\n", __func__);
        return -1;
    }

    /* Create or truncate and touch the file */
    fd = open(client_file, O_WRONLY|O_CREAT|O_TRUNC, 0644);
    if (fd == -1) {
        perror("open");
        return -1;
    }
    if (close(fd))
        perror("close");        /* Don't fail, just log error */

    return 0;
}

/**
 * Check if file exists in client directory.
 *
 * @param client_name Name of client directory to check for file in.
 * @param file_name Name of file in client directory to check for.
 *
 * @return 0 if file does not exist, 1 if file exists, -1 on error.
 */
int client_file_exists(const char *client_name, const char *file_name)
{
    const char *client_file;
    struct stat sb;

    /* Get path to file */
    client_file = client_file_path(client_name, file_name);
    if (client_file == NULL) {
        fprintf(stderr, "%s: client_file_path failed\n", __func__);
        return -1;
    }

    /* Get file status */
    if (stat(client_file, &sb) == -1) {
        if (errno == ENOMEM)
            out_of_memory();
        else if (errno == ENOENT) {
            /* File not found */
            return 0;
        }
        /* Something else failed */
        perror("stat");
        return -1;
    }

    return 1;
}

/**
 * Delete file in client directory.
 *
 * This function silently ignores it if file does not exist when called.
 *
 * @param client_name Name of client directory to delete file in.
 * @param file_name Name of file in client directory to delete.
 *
 * @return 0 if file does not exist, 1 if file exist, -1 on error.
 */
int client_file_unlink(const char *client_name, const char *file_name)
{
    const char *client_file;

    /* Get path to file */
    client_file = client_file_path(client_name, file_name);
    if (client_file == NULL) {
        fprintf(stderr, "%s: client_file_path failed\n", __func__);
        return -1;
    }

    /* Create or truncate and touch the file */
    if (unlink(client_file)) {
        if (errno == ENOENT)
            /* File not found */
            return 0;
        perror("unlink");
        return -1;
    }

    return 0;
}
