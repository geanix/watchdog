/**
 * @file
 * Watchdog client API header file.
 *
 * Header file to include in applications using the watchdog API.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef WATCHDOG_H
#define WATCHDOG_H

/**
 * @defgroup api API Functions
 * @addtogroup api
 * @{
 */

/**
 * Register with watchdog server.
 *
 * This connects to the watchdog server, and registers with the specified
 * client name and parameters.
 *
 * @param name Unique string identifying the client.
 * @param start_timeout Maximum time in milliseconds allowed before first
 *   `watchdog_keepalive()` call.
 * @param keepalive_timeout Maximum time in milliseconds allowed between calls
 *   to watchdog_keepalive().
 * @param recover_cmd Shell command to call to recover/restart the client.
 * @param recover_timeout Maximum time in milliseconds allowed between recover
 *   command and new registration.
 *
 * @return 0 on success, -1 on error (and errno is set appropriately).
 */
int watchdog_register(const char *name, unsigned start_timeout,
                      unsigned keepalive_timeout,
                      const char *recover_cmd, unsigned recover_timeout);

/**
 * Connect to watchdog service.
 *
 * This connects to the watchdog server, using the specified client name.
 * The client must be registered in advance.
 *
 * By using this call together with watchdog_register shell command, the
 * watchdog parameters can be separated from the application binary.
 *
 * If client is not already registered, you * must use watchdog_register()
 * instead.
 *
 * @param name Unique string identifying the client.
 *
 * @return handle (a non-negative number) to be used with watchdog_keepalive()
 *   on success, -1 on error (and errno is set appropriately).
 */
int watchdog_open(const char *name);

/**
 * Keepalive signal.
 *
 * Send a keepalive signal to watchdog server.
 *
 * @param handle Value returned by watchdog_register().
 *
 * @return 0 on success, -1 on error (and errno is set appropriately).
 */
int watchdog_keepalive(int handle);

/**
 * Unregister with watchdog server.
 *
 * This executes a graceful disconnect from the watchdog server.  The client
 * must call this prior to shutdown (closing of underlying file descriptors),
 * or watchdog server will assume the client have failed.
 *
 * @param name Unique string identifying the client.
 *
 * @return 0 on success, -1 on error (and errno is set appropriately).
 */
int watchdog_unregister(const char *name);

/** @} */

#endif /* WATCHDOG_H */
