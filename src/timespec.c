/**
 * @file
 * Timer related helper functions.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"

#include <stdbool.h>
#include <time.h>

#include "watchdog_int.h"

/**
 * Add time t2 to t1.
 *
 * @param t1 Time to add to.
 * @param t2 Time to add to @p t1.
 */
void timespec_add(struct timespec *t1, struct timespec *t2)
{
    t1->tv_sec += t2->tv_sec;
    t1->tv_nsec += t2->tv_nsec;
    if (t1->tv_nsec >= 1E9) {
        t1->tv_nsec -= 1E9;
        t1->tv_sec++;
    }
}

/**
 * Is t1 before t2?
 *
 * @param t1 Time to compare.
 * @param t2 Time to compare.
 * @returns true if t1 < t2, false otherwise. */
bool timespec_before(struct timespec *t1, struct timespec *t2)
{
    if (t1->tv_sec > t2->tv_sec)
        return false;
    else if (t1->tv_sec < t2->tv_sec)
        return true;
    return t1->tv_nsec < t2->tv_nsec;
}
