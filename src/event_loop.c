/**
 * @file
 * Implementation of the main (event) loop of watchdog server command.
 *
 * Used in watchdog server command.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/epoll.h>

#include "watchdog_int.h"

/**
 * Buffer size for use when reading events from epoll file descriptor.
 *
 * If more events are available at a time, they will still all be read, but
 * will just require more than one epoll_wait() call.  For performance
 * reasons, the value should be chosen so that it is large enough to normally
 * allow a single epoll_wait() call to read all queued events, but on the
 * other hand, there is no reason to waste memory.
*/
#define MAX_EVENTS 16

/**
 * An epoll file descriptor.
 *
 * This is the file descriptor being poll'ed on in event_loop().
 */
static int epollfd;

/**
 * Add file descriptor to epoll file descriptor.
 *
 * @param fd File descriptor to add to epollfd.
 * @param events Events mask to subscribe to.
 * @returns 0 on success, -1 on error.
 *
 * @sa event_loop().
 */
int event_loop_add_fd(int fd, uint32_t events)
{
    struct epoll_event ev;
    memset(&ev, 0, sizeof(ev));
    ev.events = events;
    ev.data.fd = fd;
    if (epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &ev) == -1) {
        perror("epoll_ctl");
        return -1;
    }
    return 0;
}

/**
 * Remove file descriptor from epoll file descriptor.
 *
 * @param fd File descriptor to remove from epoll instance.
 */
void event_loop_rm_fd(int fd)
{
    if (epoll_ctl(epollfd, EPOLL_CTL_DEL, fd, NULL) == -1)
        perror("epoll_ctl");
}

/**
 * The main loop of the watchdog daemon.
 *
 * This loop blocks on currently blocks on a timerfd, triggering sending of
 * keepalive signals to watchdog device.
 *
 * @param device_fd File descriptor to watchdog device or -1 for dry run mode.
 * @param device_timer_fd File descriptor to the timer object generating
 *   periodic wake-ups when keepalive signals should be sent to watchdog
 *   device.
 * @param interface_dir_fd File descriptor to inotify instance receiving
 *   filesystem events from the interface directory.
 *
 * @return EXIT_SUCCESS or EXIT_FAILURE
 */
int event_loop(int device_fd, int device_timer_fd, int interface_dir_fd)
{
    struct epoll_event events[MAX_EVENTS];

    epollfd = epoll_create1(EPOLL_CLOEXEC);
    if (epollfd == -1) {
        perror("epoll_create1");
        exit(EXIT_FAILURE);
    }

    if (event_loop_add_fd(device_timer_fd, EPOLLIN)) {
        fprintf(stderr, "%s: Adding device_timer_fd to epoll failed\n",
                __func__);
        exit(EXIT_FAILURE);
    }

    if (event_loop_add_fd(interface_dir_fd, EPOLLIN)) {
        fprintf(stderr, "%s: Adding interface_dir_fd to epoll failed\n",
                __func__);
        exit(EXIT_FAILURE);
    }

    /* Scan interface directory contents.  This is done after the interface
     * directory watch is setup, so that race conditions are in the form of
     * duplicate events in case the interface directory is modified during
     * interface_dir_init().  Also, it needs to come after epollfd is
     * created. */
    if (interface_dir_scan()) {
        fprintf(stderr, "%s: Scanning interface directory failed\n",
                __func__);
        exit(EXIT_FAILURE);
    }

    /*
     * This is the main event loop of the watchdog daemon, handling timer
     * expiration notificatons (timerfd) and filesystem events (inotify).
     *
     * Handling of these events drive transitions for all watchdog client
     * state machines and the watchdog system state machine.
     */
    while (1) {
        int nfds, n;

        nfds = epoll_wait(epollfd, events, MAX_EVENTS, -1);
        if (nfds == -1) {
            if (errno == EINTR)
                /* Temporary interruption, let's continue */
                continue;
            perror("epoll_wait");
            exit(EXIT_FAILURE);
        }

        for (n = 0; n < nfds; ++n) {
            if (events[n].data.fd == device_timer_fd) {
                /* Read out the number of timer expirations, and log it if
                 * more than 1 timer expiration happened since last time.  The
                 * watchdog server really should not get behind, as it can
                 * cause a watchdog device timeout and system reboot. */
                device_timer_event(device_timer_fd);
                /* Ensure that monitor state allows keepalive signaling */
                if (monitor_keepalive()) {
                    if (dry_run)
                        /* Silly dry-run notification on stdout */
                        printf("ping\n");
                    else
                        /* Send keepalive signal to watchdog device */
                        device_keepalive(device_fd);
                }
            } else if (events[n].data.fd == interface_dir_fd) {
                /* Process all filesystem events */
                interface_dir_handle_events();
            } else {
                client_fsm_timeout(events[n].data.fd);
            }
        }
    }

    return EXIT_SUCCESS;
}
