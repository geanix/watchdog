/**
 * @file
 * Implementation of watchdog monitor state handling.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/reboot.h>

#include "watchdog_int.h"

/** Simple finite state machine for watchdog monitor with 3 states. */
enum monitor_state {
    MONITOR_UP = 0,             /* system is up and should stay up */
    MONITOR_REBOOT,             /* system is being rebooted (PID 1 in charge) */
    MONITOR_DEAD,               /* reboot(2) syscall have been made ... */
};
/** Watchdmog monitor state. */
typedef enum monitor_state monitor_state_t;

/** The watchdog monitor state variable. */
static monitor_state_t state;

/** Time allowed for system to reboot. */
static struct timespec reboot_timeout;
/** Deadline for reboot timeout, valid (only) in REBOOT state. */
static struct timespec reboot_deadline = {0};

/**
 * Get current time and calculate reboot_deadline.
 */
static void set_reboot_deadline(void)
{
    if (clock_gettime(CLOCK_MONOTONIC, &reboot_deadline)) {
        perror("clock_gettime");
        /* This is not good.  Without knowing the current time we cannot set
         * the reboot_deadline.  But as clock_gettime(2) in
         * monitor_keepalive() will most likely also fail, we can't do much
         * else than hoping the reboot will actually succeed */
    } else {
        /* Add reboot_timeout, resulting in the deadline for when reboot must
         * have completed */
        timespec_add(&reboot_deadline, &reboot_timeout);
    }
}

/**
 * Initialize watchdog monitor.
 *
 * @param reboot_timeout_ms Reboot timeout in milliseconds.
 */
void monitor_init(unsigned long reboot_timeout_ms)
{
    reboot_timeout.tv_sec = reboot_timeout_ms / 1000;
    reboot_timeout.tv_nsec = (reboot_timeout_ms % 1000) * 1000000;

    /* Check if system reboot have already been started (by a previous
     * watchdog server instance */
    if (monitor_file_exists(".R.I.P")) {
        /* Reboot is already ongoing, so we just need to set the reboot
         * deadline.  Although it would be nice to be able to re-use the old
         * deadline, we will go with KISS principle here.  Optimizing timeouts
         * for cases where the watchdog server crashes during reboot is
         * probably not the most important thing in the world. */
        set_reboot_deadline();
        state = MONITOR_REBOOT;
        return;
    }

    state = MONITOR_UP;
}

/**
 * Dispatch client died event to monitor.
 *
 * Event function to be called from event_loop when one or more clients have
 * died.
 */
void monitor_client_died(void)
{
    if (state != MONITOR_UP)
        /* Already left UP state */
        return;

    /* Set reboot deadline relative to current time */
    set_reboot_deadline();

    /* Send SIGINT to PID 1, which should hopefully result in a system
     * reboot */
    fprintf(stderr, "%sending SIGINT to PID 1\n", dry_run ? "Not s" : "S");
    if (!dry_run)
        kill(1, SIGINT);

    /* Write hidden file so that a restart of watchdog server will know that
     * system is rebooting */
    monitor_file_write(".R.I.P.");

    state = MONITOR_REBOOT;
}

/**
 * Query monitor for keepalive status.
 *
 * This function must be called by event_loop() before each call to
 * device_keepalive().  The device_keepalive() may not be called if
 * monitor_keepalive() returns false.
 *
 * @returns true if device_keepalive() should be called, and false otherwise.
 */
bool monitor_keepalive(void)
{
    struct timespec now;

    switch (state) {
    case MONITOR_UP:
        /* Everything is looking fine, continue with watchdog device keepalive
         * signaling */
        return true;
    case MONITOR_REBOOT:
        /* Get current time */
        if (clock_gettime(CLOCK_MONOTONIC, &now)) {
            perror("clock_gettime");
            /* Let's hope that reboot succeeds */
            return true;
        }
        /* Check reboot deadline */
        if (timespec_before(&now, &reboot_deadline))
            /* We have not reached the reboot deadline yet, so the
             * watchdog device keepalive signaling must continue */
            return true;
        /* Reboot deadline passed*/
        fprintf(stderr, "%sorcing restart\n", dry_run ? "Not f" : "F");
        if (dry_run) {
            exit(EXIT_SUCCESS);
        } else {
            /* Commi`t filesystem caches to disk to avoid data loss */
            sync();
            /* Use syscall for doing an immediate reboot */
            if (reboot(RB_AUTOBOOT))
                perror("reboot");
        }
        /* Change state to DEAD and stop watchdog device keepalive
         * signaling */
        state = MONITOR_DEAD;
        return false;
    case MONITOR_DEAD:
        /* Already dead and watchdog device keepalive signaling have been
         * stopped */
        return false;
    }
    /* Never reached */
    assert(false);
    return false;
}
