/**
 * @file
 * The watchdog server command.
 *
 * This file implements main() of the `watchdog` command.
 * @sa event_loop().
 *
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

#include "watchdog_int.h"

/** Command-line usage string. */
static const char *usage = "\
Usage: watchdog [-v|--version] [-h] [-T N[ms]] [-t N[ms]] [-r N[ms]]\n\
                [-n] DEVICE\n\
\n\
User space watchdog maintaining Linux watchdog device DEVICE, and providing a\n\
fan-out style watchdog client interface.\n\
\n\
Note: The ability to set watchdog timeout is only supported by some watchdog\n\
drivers.\n\
\n\
Arguments\n\
  DEVICE   Watchdog device (i.e. /dev/watchdog)\n\
Options\n\
  -T N     Set watchdog device timeout to N seconds\n\
  -t N     Ping watchdog device every N seconds (default: timeout / 2)\n\
  -r N     Set reboot timeout to N seconds\n\
  -n       Dry-run mode, do not touch watchdog device and do not reboot\n\
  -v       Print version information on stdout and exit\n\
  -h       Display this help and exit\n\
";

/**
 * Global dry-run flag.
 *
 * This flag is set based on the -n argument given to watchdog server command,
 * and is used read-only after that where relevant.  It is implemented as a
 * global variable to avoid passing a read-only variable through a large
 * number of functions.
 */
bool dry_run = false;

/**
 * Global interface directory parameter.
 *
 * This variable is used read-only throughout the modules of the watchdog
 * server command for specifying the base directory of the interface directory
 * (i.e. /run/watchdog).
 */
const char *interface_dir = DEFAULT_INTERFACE_DIR;

/**
 * File descriptor to the watchdog device (i.e. /dev/watchdog).
 */
static int dev_fd = -1;

/**
 * Signal handler for watchdog device magic close.
 *
 * Shut down watchdog device nicely by doing a magic close.  Without this,
 * stopping the watchdog server would always keep the watchdog device
 * interface running, and thus cause watchdog timeout.
 *
 * This signal handler is installed to the same signals that busybox watchdog
 * command signal handler is installed to, making watchdog server compatible
 * with busybox watchdog command with regards to signal handling and magic
 * close.
 *
 * @param sig Signal numbr to install handler to.
 */
static void magic_close_handler(int sig)
{
    fprintf(stderr, "Got signal: %s\n", strsignal(sig));
    if (dev_fd != -1) {
        fputs("Closing watchdog device with magic\n", stderr);
        device_close(dev_fd, true);
    }
    exit(EXIT_SUCCESS);
}

/**
 * Install signal handler function to a specific signal.
 * @param sig Signal number to install signal handler to.
 * @param sa Signal handler to install.
 * @returns 0 on success, -1 on error.
 */
static int _install_signal_handler(int sig, struct sigaction *sa)
{
    if (sigaction(sig, sa, NULL) == -1) {
        perror("sigaction");
        return -1;
    }
    return 0;
}

/**
 * Install magic_close_handler() to a set of signals.
 *
 * @returns 0 on success, -1 on error.
 */
static int install_magic_close_handler(void)
{
    struct sigaction sa = {0};
    sa.sa_handler = magic_close_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    if (_install_signal_handler(SIGHUP, &sa) ||
        _install_signal_handler(SIGINT, &sa) ||
        _install_signal_handler(SIGTERM, &sa) ||
        _install_signal_handler(SIGPIPE, &sa) ||
        _install_signal_handler(SIGQUIT, &sa) ||
        _install_signal_handler(SIGABRT, &sa) ||
        _install_signal_handler(SIGVTALRM, &sa) ||
        _install_signal_handler(SIGXCPU, &sa) ||
        _install_signal_handler(SIGXFSZ, &sa) ||
        _install_signal_handler(SIGUSR1, &sa) ||
        _install_signal_handler(SIGUSR2, &sa))
        return -1;
    return 0;
}

/**
 * Install recover_cmd_zombie_handler() to SIGCHLD.
 *
 * @returns 0 on success, -1 on error.
 */
static int install_zombie_handler(void)
{
    struct sigaction sa = {0};
    sa.sa_handler = recover_cmd_zombie_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART|SA_NOCLDSTOP;
    if (_install_signal_handler(SIGCHLD, &sa))
        return -1;
    return 0;
}

/**
 * Main function of watchdog server command.
 *
 * @param argc Number of command-line arguments.
 * @param argv Command-line arguments.
 * @return EXIT_SUCCESS on success, EXIT_FAILURE on error.
 */
int main(int argc, char **argv)
{
    int opt;
    const char *dev_path;
    unsigned long dev_timeout = 0;
    unsigned long dev_interval = 0;
    unsigned long reboot_timeout = DEFAULT_REBOOT_TIMEOUT;
    int device_timer_fd, interface_dir_fd;

    /* Special handling of long-option style --version, but sticking with
     * short options for everything else */
    if (argc > 1 && (strcmp(argv[1], "--version") == 0))
        argv[1] = "-v";

    while ((opt = getopt(argc, argv, "hvT:t:r:n")) != -1) {
        switch (opt) {
        case 'h':
            fputs(usage, stdout);
            exit(EXIT_SUCCESS);
        case 'v':
            fputs("watchdog "VERSION"\n", stdout);
            exit(EXIT_SUCCESS);
        case 'T':
            if (parse_timeout_arg(&dev_timeout, optarg)) {
                perror("parse_timeout_arg: -T");
                exit(EXIT_FAILURE);
            }
            break;
        case 't':
            if (parse_timeout_arg(&dev_interval, optarg)) {
                perror("parse_timeout_arg: -t");
                exit(EXIT_FAILURE);
            }
            break;
        case 'r':
            if (parse_timeout_arg(&reboot_timeout, optarg)) {
                perror("parse_timeout_arg: -r");
                exit(EXIT_FAILURE);
            }
            break;
        case 'n':
            dry_run = true;
            break;
        default: /* '?' */
            fputs(usage, stderr);
            exit(EXIT_FAILURE);
        }
    }

    if ((argc - optind) >= 1)
        dev_path = argv[optind];
    if ((argc - optind) < 1) {
        fputs("DEVICE argument missing\n", stderr);
        fputs(usage, stderr);
        exit(EXIT_FAILURE);
    } else if ((argc - optind) > 1) {
        fputs("too many arguments\n", stderr);
        fputs(usage, stderr);
        exit(EXIT_FAILURE);
    }

    if (!dry_run) {
        /* Install signal handler to do magic close style shutdown first */
        if (install_magic_close_handler())
            return -1;
        /* And with that in place, open watchdog device */
        fprintf(stderr, "opening watchdog device %s\n", dev_path);
        dev_fd = device_open(dev_path);
    }

    /* Install signal handler to reap recover command zombies */
    if (install_zombie_handler())
        return -1;

    /* If watchdog device supports WDIOC_SETTIMEOUT and WDIOC_GETTIMEOUT */
    if (!dry_run && device_supports_settimeout(dev_fd)) {
        /* Set or get watchdog device timeout */
        if (dev_timeout) {
            fprintf(stderr, "setting watchdog device timeout to %lu ms\n",
                    dev_timeout);
            device_settimeout(dev_fd, dev_timeout);
        }
        /* Always read timeout, even when it was just set, as it might not be
         * set to the precise value given. Currently, watchdog device API only
         * supports seconds (i.e. milliseconds are truncated). */
        dev_timeout = device_gettimeout(dev_fd);
        fprintf(stderr, "watchdog device timeout is %lu ms\n", dev_timeout);
        /* Apply default keepalive interval if not specified */
        if (dev_interval == 0)
            dev_interval = dev_timeout / 2;
    } else {
        /* We cannot set timeout, and need to have keepalive interval
         * specified */
        if (dev_timeout) {
            fputs("WDIOC_SETTIMEOUT not supported, -T argument not supported\n",
                  stderr);
            exit(EXIT_FAILURE);
        }
        if (!dev_interval) {
            fputs("WDIOC_GETTIMEOUT not supported, -t argument required\n",
                  stderr);
            exit(EXIT_FAILURE);
        }
    }

    if (dev_interval == 0) {
        dev_interval = dev_timeout / 2;
    }

    device_timer_fd = device_timer(dev_interval);
    if (device_timer_fd == -1) {
        fprintf(stderr, "%s: Error creating device timer\n", __func__);
        exit(EXIT_FAILURE);
    }

    interface_dir_fd = interface_dir_init();
    if (interface_dir_fd == -1) {
        fprintf(stderr, "%s: Error initializing interface directory\n",
                __func__);
        exit(EXIT_FAILURE);
    }

    monitor_init(reboot_timeout);

    if (!dry_run)
        fprintf(stderr, "Pinging watchdog device %s every %lu ms\n",
                dev_path, dev_interval);
    else
        fprintf(stderr, "Not pinging watchdog device every %lu ms\n",
                dev_interval);

    return event_loop(dev_fd, device_timer_fd, interface_dir_fd);
}
