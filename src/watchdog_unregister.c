/**
 * @file
 * The watchdog_unregister shell command.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "watchdog.h"

/** Command-line usage string. */
static const char *usage = "\
Usage: watchdog_unregister [-v|--version] [-h] NAME\n\
\n\
Unregister a watchdog client with the watchdog server.\n\
\n\
After unregistering, the application can shutdown without watchdog server\n\
taking any further actions.\n\
\n\
Arguments\n\
  NAME     Client name to register\n\
Options\n\
  -v       Print version information on stdout and exit\n\
  -h       Display this help and exit\n\
";

/**
 * Main function of watchdog_register client command.
 *
 * @param argc Number of command-line arguments.
 * @param argv Command-line arguments.
 * @return EXIT_SUCCESS on success, EXIT_FAILURE on error.
 */
int main(int argc, char **argv)
{
    int opt;
    const char *name;

    /* Special handling of long-option style --version, but sticking with
     * short options for everything else */
    if (argc > 1 && (strcmp(argv[1], "--version") == 0))
        argv[1] = "-v";

    /* Parse command-line arguments for options */
    while ((opt = getopt(argc, argv, "hv")) != -1) {
        switch (opt) {
        case 'h':
            fputs(usage, stdout);
            exit(EXIT_SUCCESS);
        case 'v':
            fputs("watchdog "VERSION"\n", stdout);
            exit(EXIT_SUCCESS);
        default: /* '?' */
            fputs(usage, stderr);
            exit(EXIT_FAILURE);
        }
    }

    /* Parse remaining command-line arguments */
    if ((argc - optind) >= 1)
        name = argv[optind];
    if ((argc - optind) < 1) {
        fputs("NAME argument missing\n", stderr);
        fputs(usage, stderr);
        exit(EXIT_FAILURE);
    } else if ((argc - optind) > 1) {
        fputs("too many arguments\n", stderr);
        fputs(usage, stderr);
        exit(EXIT_FAILURE);
    }

    /* Call the API function */
    if (watchdog_unregister(name))
        /* API function will print out error message to stderr */
        return EXIT_FAILURE;
    else
        return EXIT_SUCCESS;
}
