/**
 * @file
 * Handling of watchdog device (/dev/watchdog).
 *
 * Used in watchdog server command.
 */

/* This file is part of User Space Watchdog.
 *
 * Copyright 2018 DEIF A/S.
 * Author: Esben Haabendal <esben@geanix.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/timerfd.h>
#include <sys/ioctl.h>
#include <linux/watchdog.h>

#include "watchdog_int.h"

/**
 * Open watchdog device.
 *
 * This function exits the program with EXIT_FAILURE on error.
 *
 * @param path Path to watchdog device (i.e. /dev/watchdog).
 * @returns File descriptor.
 */
int device_open(const char *path)
{
    int fd = open(path, O_RDWR|O_CLOEXEC);
    if (fd == -1) {
        fprintf(stderr, "open(%s): %s\n", path, strerror(errno));
        exit(EXIT_FAILURE);
    }
    return fd;
}

/**
 * Close watchdog device.
 *
 * If @p magic is true, a 'V' character is written before close() (i.e. magic
 * close).
 *
 * @param fd File descriptor to watchdog device.
 * @param magic Set to true to do magic close.
 */
void device_close(int fd, bool magic)
{
    static const char magic_v = 'V';
    if (magic && write(fd, &magic_v, 1) == -1)
        perror("write: magic character");
    if (close(fd) == -1)
        perror("close");
}

/**
 * Check if watchdog device supports WDIOC_SETTIMEOUT/WDIOC_GETTIMEOUT.
 *
 * This function exits the program with EXIT_FAILURE on error.
 *
 * @param fd File descriptor to watchdog device.
 * @returns true if device supports WDIOC_SETTIMEOUT (and WDIOC_GETTIMEOUT),
 *   false otherwise.
 */
bool device_supports_settimeout(int fd)
{
    struct watchdog_info ident;
    if (ioctl(fd, WDIOC_GETSUPPORT, &ident)) {
        perror("ioctl: WDIOC_GETSUPPORT");
        exit(EXIT_FAILURE);
    }
    return !!(ident.options & WDIOF_SETTIMEOUT);
}

/**
 * Set watchdog device timeout.
 *
 * This function exits the program with EXIT_FAILURE on error.
 *
 * This sets the timeout of the watchdog device.  This is the time allowed
 * between keepalive signals.  The system will reboot if this timeout occurs.
 *
 * @param fd File descriptor to watchdog device.
 * @param timeout_ms New timeout value in milliseconds.
 */
void device_settimeout(int fd, unsigned long timeout_ms)
{
    int timeout = timeout_ms / 1000;
    if (ioctl(fd, WDIOC_SETTIMEOUT, &timeout)) {
        perror("ioctl: WDIOC_SETTIMEOUT");
        exit(EXIT_FAILURE);
    }
}

/**
 * Get watchdog device timeout.
 *
 * This function exits the program with EXIT_FAILURE on error.
 *
 * @param fd File descriptor to watchdog device.
 * @returns Timeout value in milliseconds.
 */
unsigned long device_gettimeout(int fd)
{
    int timeout = 0;
    if (ioctl(fd, WDIOC_GETTIMEOUT, &timeout)) {
        perror("ioctl: WDIOC_GETTIMEOUT");
        exit(EXIT_FAILURE);
    }
    return timeout * 1000;
}

/**
 * Send keepalive signal to watchdog device.
 *
 * An error message is printed in case of failure to send keepalive signal.
 *
 * @param fd File descriptor to watchdog device.
 */
void device_keepalive(int fd)
{
    if (ioctl(fd, WDIOC_KEEPALIVE, 0))
        /* Just print error message, but try to continue.  The watchdog device
         * (i.e. the hardware watchdog) is responsible for pulling the big
         * switch anyway */
        perror("ioctl: WDIOC_KEEPALIVE");
}

/**
 * Create periodic watchdog timer object.
 *
 * This function creates a new timer (timerfd) object, and configures and arms
 * it as an interval timer.  On success, the timer will be generating a timer
 * event every @p interval milliseconds.
 *
 * @param interval Number of millisconds between timer events.
 * @returns File descriptor on success, -1 on error.
 */
int device_timer(unsigned long interval)
{
    struct itimerspec timer_spec = {0};
    int fd;

    fd = timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC|TFD_NONBLOCK);
    if (fd == -1) {
        perror("timerfd_create");
        return -1;
    }

    timer_spec.it_interval.tv_sec = interval / 1000;
    timer_spec.it_interval.tv_nsec = (interval % 1000) * 1000000;
    timer_spec.it_value.tv_sec = timer_spec.it_interval.tv_sec;
    timer_spec.it_value.tv_nsec = timer_spec.it_interval.tv_nsec;
    if (timerfd_settime(fd, 0, &timer_spec, NULL)) {
        perror("timerfd_settime");
        close(fd);
        return -1;
    }

    return fd;
}

/**
 * Consume peridic timer event.
 *
 * An error message is printed if the number of timer events (expirations)
 * could not be read.
 *
 * An info message is printed if more than 1 timer events (expirations) were
 * read.  By itself, this does not cause problems, but as it could easily
 * cause watchdog device timeouts and thus system reboot, it should not be
 * ignored, but we cannot really do much about it.
 *
 * @param fd File descriptor returned by device_timer().
 */
void device_timer_event(int fd)
{
    uint64_t expirations = 0;

    /* Read number of expirations */
    if (timerfd_read_expirations(fd, &expirations)) {
        fprintf(stderr, "%s: Error reading timerfd object\n", __func__);
        return;
    }

    /* Log message when missing timer events. */
    if (expirations > 1)
        fprintf(stderr, "%s: Missed %d device timer events",
                __func__, (int)(expirations - 1));
}
