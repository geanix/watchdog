Design
======

The design is basically a client server design.  A process (watchdog) is the
server, and a number of (0 .. N) clients are registered with and monitored by
the server.


Interface directory
-------------------

The interface between watchdog server and clients are based on plain files and
directories in tmpfs (`/run/watchdog` by default).  This is called the
interface directory.

For each client, a sub-directory is created in the interface directory, so
that for example client "foo" uses `/run/watchdog/foo`.  These
sub-directories are called client directories.  The client direcotry is
created on registration if it does not already exist.

Inotify
~~~~~~~

The server uses :manpage:`inotify(7)` to receive notification on changes in
the interface directory.

When the server is notified of a new directory, it is added to the inotify
watch, and then immediately scanned for the current content.  A watchdog
client state machine (see :ref:`client-fsm`) is created and the state is set
based on the files found by the scan (see table :ref:`state-file-table`).

Security concerns
~~~~~~~~~~~~~~~~~

In order to avoid security concerns related to a generally writable
`/run/watchdog`, it could be pre-populated with directories for the clients
that are allowed access, with proper file permission or ACL setup in advance.
This way only authorized applications (users) will be able to interface with
watchdog server, and thus cause a system reboot.


Client interface
----------------

The interface between watchdog server and clients is based on 4 discrete
actions:

1. Registration
2. Open
3. Keepalive
4. Unregistration

Registration
~~~~~~~~~~~~

The first thing an application need to do is to register with the watchdog
server.  By doing this registration, the application becomes a watchdog
client.

A watchdog client is identified by a name, which must be a unique string.  It
is up to the application developer(s) to coordinate the client names.

Registration can be done using either the :func:`watchdog_register()` C
function or the `watchdog_register` shell command.

Internally, the registration is done by writing a registration file for the
client.  The registration file is placed in a client specific sub-directory of
`/run/watchdog`.

The implementation takes care to create the file in a way so that it is either
atomically created atomically or replaces the existing file atomically.  This
is necessary in order to avoid race conditions where a partially written
registration file is parsed by the watchdog server, and in order to avoid a
temporary removal of the registration file.

Additional client created file ("ping" and "recover") are removed on
registration, so a restart of the watchdog server can identify the current
client state (see table :ref:`state-file-table`).

When a client have registered, it must call :func:`watchdog_open()` (or the
`watchdog_keepalive` shell command) before a timeout as specified by start
timeout parameter (see :func:`watchdog_register()`).

Open
~~~~

When an application enters its operation mode, it should call
:func:`watchdog_open()`, signaling to the server that it want to entire ALIVE
state.

The open call creates a "ping" file in the client directory, and a file
descriptor to this file is returned for use in :func:`watchdog_keepalive()`
calls.

There is no shell command corresponding to :func:`watchdog_open()`.  Shell
scripts should simply use `watchdog_keepalive` which implies open.

Keepalive
~~~~~~~~~

The :func:`watchdog_keepalive()` call signals that the client is still alive.

Watchdog clients must send keepalive signals to the server with a maximum
interval as specified by the keepalive timeout parameter (see
:func:`watchdog_open()`).

Keepalive signals are send with :func:`watchdog_keepalive()` or the
`watchdog_keepalive` shell command.

In :func:`watchdog_keepalive()`, the client maintains the "ping" file in the
client directory.  It basically calls :manpage:`futimens(2)` on the file
descriptor returned by :func:`watchdog_open()`. This way, only a single
syscall is made, keeping the overhead caused by :func:`watchdog_keepalive()`
low.

Unregistration
~~~~~~~~~~~~~~

If the application wants to stop sending keepalive signals and shutdown, it
must call :func:`watchdog_unregister()` (or the `watchdog_unregister` shell
command) after the last keepalive signal.  The unregistration must be within
the timeout specified by keepalive timeout!

Internally, the unregistration is done by removing the registration file,
which is the event that the watchdog server receives.

If a client stops keepalive signals without this, the system will be rebooted.

.. _client-fsm:

Client state machine
--------------------

The state of a watchdog client is modelled as a state machine with 5 states:

* READY
* START
* ALIVE
* RECOVER
* DEAD

When watchdog server is started, it will scan `/run/watchdog` and identify all
current clients, and determine their current state based on the content of
their client interface directory (i.e. `/run/watchdog/foo` for client "foo").

When a client state machine is created, the client interface directory is
added to the inotify watch of the watchdog server.

The following table shows how the state is determined, based on the presence
of four files: "registration", "ping", "recover", "R.I.P".

.. _state-file-table:

.. table:: Client state files

   ======= ================= ========= ============ ===========
   State   Registration file Ping file Recover file R.I.P. file
   ======= ================= ========= ============ ===========
   READY   no                          no           no
   START   yes               no        no           no
   ALIVE   yes               yes       no           no
   RECOVER                             yes          no
   DEAD                                             yes
   ======= ================= ========= ============ ===========

The following figure shows the state transition diagram for the client state
machine:

.. digraph:: watchdog_client_fsm

    rankdir="LR";
    size="5.4"

    node [shape = doublecircle]; READY; DEAD;
    node [shape = point ]; qi;

    node [shape = circle];

    qi -> READY;

    READY -> START [ label = "register" ];

    START -> DEAD [ label = "timeout" ];
    START -> ALIVE [ label = "open" ];
    START -> READY [ label = "unregister" ];
    START -> START [ label = "register" ];

    ALIVE -> RECOVER [ label = "timeout [recover_cmd]" ];
    ALIVE -> DEAD [ label = "timeout [!recover_cmd]" ];
    ALIVE -> ALIVE [ label = "keepalive" ];
    ALIVE -> READY [ label = "unregister" ];
    ALIVE -> START [ label = "register" ];

    RECOVER -> DEAD [ label = "timeout" ];
    RECOVER -> START [ label = "register" ];
    RECOVER -> READY [ label = "unregister" ];

READY
~~~~~

The READY state represents clients which client interface directory is being
watched by :manpage:`inotify(7)`, but which are currently not registered.

In all other states (except for DEAD), a transition to READY is triggered by
the deletion of the registration file.

There is no timeout in READY.

START
~~~~~

The START state represents clients which have called
:func:`watchdog_register()` but not yet :func:`watchdog_open()` (and thus not
:func:`watchdog_keepalive()`, as it requires a call to :func:`watchdog_open()`
first).

On a transition from another state to START, a timer is started with timeout
as specified by `start_timeout`.  Transitions from START to START does not
reset the timer, but modifies the timeout if `start_timeout` is changed.

Timeout triggers a transition to DEAD.

ALIVE
~~~~~

The ALIVE state represents clients that have registered and have called
:func:`watchdog_open()`.

On transition to ALIVE, a timer is started with timeout as specified by
`keepalive_timeout`.  Transition from ALIVE to ALIVE re-starts the timer.

The server will be notified of the `watchdog_keepalive()` calls in the form of
`IN_ATTRIB` inotify events.

Timeout triggers a transition to RECOVER or DEAD.  The transition will be to
RECOVER if a `recover_cmd` is specified, and DEAD otherwise.

RECOVER
~~~~~~~

The RECOVER state represents clients that have missed their
`keepalive_timeout` deadline, and where a `recover_cmd` have been specified.

On transition to RECOVER, a timer is started with timeout as specified by
`recover_timeout`.

On the transition to RECOVER, the `recover_cmd` is run (fork + exec).  The
purpose of the `recover_cmd` is to get the client to call
:func:`watchdog_register()` and then :func:`watchdog_keepalive()` again.

This could for example be achieved by terminating the application and rely on
a process supervisor (e.g. s6-supervise) start the application again.  But the
watchdog server as such does not care about how this is achieved.  The
watchdog server only handles the files in `/run/watchdog` and not any other
type of IPC.

DEAD
~~~~

The DEAD state represents clients which watchdog server have given up on.

When server has at least one DEAD client, system reboot is started.


Monitor state machine
---------------------

To handle the overall watchdog state, a state machine called monitor is used.

The watchdog monitor state machine has only 3 different states

* UP
* REBOOT
* DEAD

.. digraph:: monitor_fsm

    rankdir="LR";
    size="5.4"
    node [shape = doublecircle]; UP; DEAD;
    node [shape = point ]; qi;
    node [shape = circle];
    qi -> UP;
    UP -> REBOOT [ label = "client_died" ];
    REBOOT -> DEAD [ label = "reboot_timeout" ];

UP
~~

The UP state is the initial state.  The monitor stays in UP state as long as
no watchdog clients are declared DEAD.

When a watchdog client enters DEAD state, the monitor state is changed to
REBOOT.

The monitor sends keepalive to the watchdog device while in UP state.

REBOOT
~~~~~~

When entering REBOOT, a system reboot is started, and a timer is started
(`reboot_timeout`).

This is done by sending a SIGINT signal to PID 1 (the init process).

If the system is still there after `reboot_timeout`, the monitor state is
changed to DEAD.

The monitor sends keepalive to the watchdog device while in REBOOT state.

DEAD
~~~~

When entering DEAD, an immediate reboot is done using :manpage:`reboot(2)`.

The monitor does **not** send keepalive signals in DEAD.

Thus, any sane system will reboot up entry to monitor DEAD state.


Logging
-------

The watchdog server prints log message to stderr.  OS integration should take
care to redirect stderr to log files.
