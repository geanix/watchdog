API
===

.. highlight:: c
   :linenothreshold: 5

The client API provides a mechanism for applications to be monitored by the
watchdog server.

The client API is provided both as a C library and a few shell commands.

The C API enables applications written in C to easily connect to and be
monitored by the watchdog server.

The API consists of a few simple functions.  To use them, application must
include `watchdog.h` and link with libwatchdog (`-lwatchdog`).

The shell command-line interface consists of a few simple commands, which can
either be used by themselves, or in combination with the C API.

Examples
--------

The following examples should give give a quick overview of how to use the C
API and shell commands.

C API example
~~~~~~~~~~~~~~

Example showing how to use the C API:

.. code-block:: c
   :emphasize-lines: 6,8,10,13
   :linenos:

   #include <watchdog.h>

   int main(int argc, char **argv)
   {
       int wd;
       watchdog_register("foobar", 10000, 1000, NULL, 0);
       // initialize some application stuff
       wd = watchdog_open("foobar");
       while (1) {
           watchdog_keepalive(wd);
           // do some application stuff ...
           if (something_failed()) {
               watchdog_unregister("foobar");
               // cleanup application stuff
               return 1;
           }
       }
       return 0;
   }

The above example registers with watchdog server as watchdog client "foobar",
requesting a start timeout of 10 seconds, a keepalive timeout of 1 second, and
no recover command (line 6).

After initializing application stuff, the watchdog is opened (line 8).  If
more than 10 seconds passes between line 6 and 8, the watchdog server will
reboot the system.

A keepalive signal is sent (line 10) inside the main loop.  If a loop
iteration takes more than 1 second, the watchdog server will reboot the
system.

If something goes wrong, the watchdog server is notified that the client is
shutting down (line 13).  Without this, the watchdog server would enforce the
keepalive timeout, and the system will reboot 1 second after start of last
loop iteration.

A proper application should of-course check the return value of
:func:`watchdog_register()`, :func:`watchdog_open()`,
:func:`watchdog_keepalive()`, and :func:`watchdog_unregister()` for errors.


Shell command example
~~~~~~~~~~~~~~~~~~~~~

Example showing how to use the shell commands:

.. code-block:: sh
   :emphasize-lines: 3,7,11
   :linenos:

   #!/bin/sh

   watchdog_register -s 10 -t 1 foobar
   # initialize some application stuff
   while true
   do
       watchdog_keepalive foobar
       # do some application stuff
       if something_bad_happened
       then
           watchdog_unregister foobar
           exit 1
       fi
   done

The above example registers with watchdog server as watchdog client "foobar",
requesting a start timeout of 10 seconds, a keepalive timeout of 1 second, and
no recover command (line 3).

A keepalive signal is sent (line 7) inside the main loop.  If the first loop
iteration is not started within 10 seconds after registration (i.e. between
line 3 and 7), the watchdog server will reboot the system.  If a loop
iteration takes more than 1 second, the watchdog server will reboot the
system.

If something goes wrong, the watchdog server is notified that the client is
shutting down (line 11).  Without this, the watchdog server would enforce the
keepalive timeout, and the system will reboot 1 second after start of last
loop iteration.

A proper application should of-course check the exit code of
`watchdog_register`, `watchdog_keepalive`, and `watchdog_unregister` for
errors.


Combined example
~~~~~~~~~~~~~~~~

Example how to use the C API together with the `watchdog_register` shell
command.

If an application is run under process supervision (e.g. runit or s6), the use
of the `watchdog_register` shell command instead of the
:func:`watchdog_register()` function might be preferred.

By running `watchdog_register` shell command before starting the application,
the watchdog server is able to make sure that application actually launches.
Errors such as missing run-time libraries will result in watchdog timeout
(start timeout).

A ./run script could look like this:

.. code-block:: sh
   :emphasize-lines: 2
   :linenos:

   #!/bin/sh
   watchdog_register -s 10 -k 1 foobar
   ./foobar

And the C application could be something like:

.. code-block:: c
   :emphasize-lines: 7,9
   :linenos:

   #include <watchdog.h>

   int main(int argc, char **argv)
   {
       int wd;
       // initialize some application stuff
       wd = watchdog_open("foobar");
       while (1) {
           watchdog_keepalive(wd);
           // do some application stuff ...
       }
       return 0;
   }


watchdog_register()
-------------------

An application call :func:`watchdog_register()` to connect to and register
with the watchdog daemon.

After this, the application must call :func:`watchdog_open()` and then a
continuous series of :func:`watchdog_keepalive()` calls with a maximum
interval of `keepalive_timeout` milliseconds.  The :func:`watchdog_open()`
call must come after maximum `start_timeout` milliseconds.

If the `keepalive_timeout` deadline is missed, the `recover_cmd` (shell
command) is executed if not NULL, and :func:`watchdog_keepalive()` must start
again after maximum `recover_timeout` milliseconds.  The `recover_cmd` is
executed with same UID as the owner of the registration file.

If `recover_cmd` is not specified, or if the `recover_timeout` deadline is
missed, the system is rebooted.

.. doxygenfunction:: watchdog_register


watchdog_open()
---------------

An application must call :func:`watchdog_open()` after it has registered with
the watchdog server.  Failing to do so before `start_timeout` expires will
cause the system to reboot.

The call to :func:`watchdog_open()` must be followed by a call to
:func:`watchdog_keepalive()` within the time specified by `keepalive_timeout`.

.. doxygenfunction:: watchdog_open


watchdog_keepalive()
--------------------

An application must call :func:`watchdog_keepalive()` at least every
`keepalive_timeout` milliseconds.  Failing to do so will cause the system to
be rebooted.

.. doxygenfunction:: watchdog_keepalive


watchdog_unregister()
---------------------

An application may call :func:`watchdog_unregister()` at any time after
calling :func:`watchdog_register()`.  After calling
:func:`watchdog_unregister()`, the application will not be registered as a
watchdog client anymore, and should not do keepalive signaling anymore.

.. doxygenfunction:: watchdog_unregister


watchdog_register command
-------------------------

The `watchdog_register` shell command is a simple wrapper around
:func:`watchdog_register()`.

.. code-block:: text

   Usage: watchdog_register [-v|--version] [-h] [-s N[ms]] [-k N[ms]]
                            [-R CMD -r N[ms]] NAME

   Register a watchdog client with the watchdog server.

   After registering, keepalive signals must be sent using watchdog_keepalive
   command or the watchdog_keepalive() API.

   Arguments
     NAME     Client name to register
   Options
     -s N     Maximum time in seconds (milliseconds if prefixed with ms) before
                first keepalive (default: 60000 ms)
     -k N     Maximum time in seconds (milliseconds if prefixed with ms) allowed
                between keepalive signals (default: 1000 ms)
     -R CMD   Command to execute for recovering the client
     -r N     Maximum time in seconds (milliseconds if prefixed with ms) allowed
                between recover command and new registration
     -v       Print version information on stdout and exit
     -h       Display this help and exit


watchdog_keepalive command
--------------------------

The `watchdog_keepalive` shell command is a wrapper around
:func:`watchdog_open()` and :func:`watchdog_keepalive()`.

.. code-block:: text

   Usage: watchdog_keepalive [-v|--version] [-h] NAME

   Send watchdog client keepalive signal to watchdog server.

   Arguments
     NAME     Client name to use
   Options
     -v       Print version information on stdout and exit
     -h       Display this help and exit

watchdog_unregister command
---------------------------

The `watchdog_unregister` shell command is a simple wrapper around
:func:`watchdog_unregister()`.

.. code-block:: text

   Usage: watchdog_unregister [-v|--version] [-h] NAME

   Unregister a watchdog client with the watchdog server.

   After unregistering, the application can shutdown without watchdog server
   taking any further actions.

   Arguments
     NAME     Client name to register
   Options
     -v       Print version information on stdout and exit
     -h       Display this help and exit
