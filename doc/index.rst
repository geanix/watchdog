.. User Space Watchdog documentation master file, created by
   sphinx-quickstart on Fri Feb  9 09:45:37 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation of User Space Watchdog
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   design
   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
