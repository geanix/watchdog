A watchdog service, responsible for maintaining a Linux watchdog device
(/dev/watchdog) and monitoring user space applications and services.

In it's simplest use-case, it behaves like the busybox watchdog command, and
thus services as a simple monitor of basic user space responsiveness.
But in addition to that, watchdog also supports monitoring any number of user
space applications or services.

An application registers itself as a watchdog client to be monitored by the
server, and is thereafter required to send periodic keepalive signals to the
server.  The server makes sure that the application starts up, and stays
alive.  In case the application fails (e.g. hangs or crashes), the watchdog
server can initiate recovery actions (such as restarting the application), via
an application specified command, and finally, if that fails, reboot the
entire system.

See documentation for usage examples and design and API details.
