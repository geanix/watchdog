#!/bin/sh

usage() {
  echo "
Usage: $0 <OPTIONS> [COMMAND]*
Options:
  -h | --help       Prints this message
Commands:
  bootstrap
  configure_host
  compile
  install [DESTDIR]
  clean
  distclean
"
}

bootstrap() {
  ./bootstrap.sh
}

configure() {
  target=${1:-host}
  rm -f .destdir
  case "$target" in
    host)
      ./configure --prefix=`pwd`/build/host
      ;;
    *)
      echo >stderr "unsupported target: $target"
      exit 1
  esac
}

compile() {
  make all
}

install() {
  if [ -e .destdir ] ; then
    make install DESTDIR="$(cat .destdir)"
  else
    make install
  fi
}

doc() {
  cd doc
  make all
}

clean() {
  make clean
}

distclean() {
  make distclean
}

if [ $# -eq 0 ] ; then
  usage; exit 1
fi
while getopts h OPTKEY ; do
  case "$OPTKEY" in
    h) usage; exit 0;;
    ?) usage; exit 2;;
  esac
done
shift $(($OPTIND - 1))
command=$(echo $1|tr '-' '_') ; shift

"$command" "$@"
