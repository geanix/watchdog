#!/bin/sh
#
# This script prints a complete revision string based on information
# from git.
#

usage() {
	echo "Usage: $0 [srctree]" >&2
	exit 1
}

cd "${1:-.}" || usage
git describe --dirty --always | sed -E 's/^v([0-9])/\1/' | tr -d '\n'
